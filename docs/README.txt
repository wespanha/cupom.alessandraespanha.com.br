README
======

This directory should be used to place project specfic documentation including
but not limited to project notes, generated API/phpdoc documentation, or
manual files generated or hand written.  Ideally, this directory would remain
in your development environment only and should not be deployed with your
application to it's final production location.


Setting Up Your VHOST
=====================

The following is a sample VHOST you might want to consider for your project.

<VirtualHost *:80>
   DocumentRoot "C:/wamp/www/projetoZendFramework/public"
   ServerName local.projetozendframework

   # This should be omitted in the production environment
   SetEnv APPLICATION_ENV development

   <Directory "C:/wamp/www/projetoZendFramework/public">
       Options Indexes MultiViews FollowSymLinks
       AllowOverride All
       Order allow,deny
       Allow from all
   </Directory>

</VirtualHost>

Banco de dados
===============

-- phpMyAdmin SQL Dump
-- version 4.6.5.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 10-Ago-2017 às 17:28
-- Versão do servidor: 5.7.16
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cupons.alessandraespanha.com.br`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `admin_user`
--

CREATE TABLE `admin_user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` enum('y','n') NOT NULL DEFAULT 'y'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `atendentes`
--

CREATE TABLE `atendentes` (
  `id` int(11) NOT NULL,
  `idParceiro` int(11) NOT NULL,
  `nomeDeUsuario` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `atendentes`
--

INSERT INTO `atendentes` (`id`, `idParceiro`, `nomeDeUsuario`, `senha`) VALUES
(1, 1, 'fernando', '12345678');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `icone` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `slug`, `nome`, `icone`) VALUES
(1, 'automotivo', 'Automotivo', '/images/categorias/automotivo.png'),
(2, 'beleza', 'Beleza', '/images/categorias/beleza.png'),
(3, 'educacao', 'Educação', '/images/categorias/educacao.png'),
(4, 'moda', 'Moda', '/images/categorias/moda.png'),
(5, 'restaurante', 'Restaurante', '/images/categorias/restaurante.png'),
(6, 'bemestar', 'Bem Estar', '/images/categorias/bemestar.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cupons`
--

CREATE TABLE `cupons` (
  `id` int(11) NOT NULL,
  `idParceiro` int(11) NOT NULL,
  `breve_descricao` text NOT NULL,
  `detalhes` text NOT NULL,
  `de` varchar(50) DEFAULT NULL,
  `por` varchar(50) DEFAULT NULL,
  `locais` text NOT NULL,
  `quantidade` int(11) NOT NULL,
  `categoria` varchar(100) NOT NULL,
  `imagem` text NOT NULL,
  `data_inicio` date NOT NULL,
  `data_termino` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cupons`
--

INSERT INTO `cupons` (`id`, `idParceiro`, `breve_descricao`, `detalhes`, `de`, `por`, `locais`, `quantidade`, `categoria`, `imagem`, `data_inicio`, `data_termino`) VALUES
(1, 1, 'Desconto de 70%', 'Desconto de 70% na peça apresentada na imagem.', '120,00', '84,90', 'Loja HOPE Via Café Garden Shopping', 1, 'moda', '/images/cupons/hope.jpg', '2017-07-07', '2017-09-06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cupons_participantes`
--

CREATE TABLE `cupons_participantes` (
  `id` int(11) NOT NULL,
  `idCupom` int(11) NOT NULL,
  `idParticipante` int(11) NOT NULL,
  `rand` int(30) NOT NULL,
  `validado` enum('s','n') NOT NULL DEFAULT 'n',
  `data` date NOT NULL,
  `data_validado` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cupons_participantes`
--

INSERT INTO `cupons_participantes` (`id`, `idCupom`, `idParticipante`, `rand`, `validado`, `data`, `data_validado`) VALUES
(10, 1, 10, 2358, 'n', '2017-08-10', NULL),
(11, 1, 11, 78460, 's', '2017-08-10', '2017-08-10');

-- --------------------------------------------------------

--
-- Estrutura da tabela `parceiros`
--

CREATE TABLE `parceiros` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `parceiros`
--

INSERT INTO `parceiros` (`id`, `nome`, `logo`) VALUES
(1, 'HOPE Via Café Garden Shopping', '/images/parceiros/hope.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `participantes`
--

CREATE TABLE `participantes` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `celular` varchar(100) NOT NULL,
  `cpf` varchar(100) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `participantes`
--

INSERT INTO `participantes` (`id`, `nome`, `email`, `celular`, `cpf`, `data`) VALUES
(10, 'Fernando Moscardini', 'fdmoscardini@gmail.com', '(35) 98889-3639', '129.075.216-83', '2017-08-10'),
(11, 'Fernando Moscardini 2', 'wespanha@wespanha.com.br', '(35) 98889-3639', '129.075.216-82', '2017-08-10'),
(12, 'Fernando Moscardini 3', 'fernandomoscardini2@hotmail.com', '(35) 98889-3639', '121.321.321-32', '2017-08-10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_user`
--
ALTER TABLE `admin_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atendentes`
--
ALTER TABLE `atendentes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cupons`
--
ALTER TABLE `cupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cupons_participantes`
--
ALTER TABLE `cupons_participantes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parceiros`
--
ALTER TABLE `parceiros`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participantes`
--
ALTER TABLE `participantes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_user`
--
ALTER TABLE `admin_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `atendentes`
--
ALTER TABLE `atendentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cupons`
--
ALTER TABLE `cupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cupons_participantes`
--
ALTER TABLE `cupons_participantes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `parceiros`
--
ALTER TABLE `parceiros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `participantes`
--
ALTER TABLE `participantes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

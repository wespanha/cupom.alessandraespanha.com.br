<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    /**
     * Inicializa o banco de dados. Somente necessario se deseja salvar a conexao no registro
     * @name _initDB
     * @return void
     */
    public function _initDB() {
        $db = $this->getPluginResource('db')->getDbAdapter();
        Zend_Db_Table::setDefaultAdapter($db);
        Zend_Registry::set('db', $db);
    }

    /**
     * Inicializa o Namespace do plugin Acl
     * @name _initAutoLoader
     * @return void
     */
    protected function _initAutoloader(){
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('Plugins');
    }

    /**
     * Inicializa o Plugin App/Plugin/Acl.php
     * @name _initPlugins
     * @return void
     */
    protected function _initPlugins(){
        $bootstrap = $this->getApplication();
        if ($bootstrap instanceof Zend_Application) {
            $bootstrap = $this;
        }
        $bootstrap->bootstrap('FrontController');
        $front = $bootstrap->getResource('FrontController');

        $front->registerPlugin(new Plugins_Auth_Acl());

    }

    /**
     * Função para carregar automaticamente o diretorio de layout's para cada modulo
     * @name _initLayout
     * @return void
     */
    public function _initLayout() {
        $request_uri = explode('/', $_SERVER['REQUEST_URI']);
        $modules = array('main','admin');
        $layout = "main";

        foreach($modules as $key => $value) {
            (in_array($value, $request_uri)) ? $layout = $value : '';
        }

        $option = array(
            'layout'        => 'layout',
            'layoutPath' => APPLICATION_PATH . "/modules/". $layout . "/layouts"
        );

        Zend_Layout::startMvc($option);
    }
    
    /**
     * Função responsável por conectar a api do sistema
     * @name _initConexaoWs
     * @return void
     */
    public function _initConexaoWs() {
        try {

            //instacia o serviço http
            $http = new Zend_Http_Client();

            $http->setHeaders('Accept','application/json;odata=verbose');
            $http->setHeaders('Content-Type','application/json; charset=utf8');
            
            // Registra o webservices
            $registry = Zend_Registry::getInstance();
            $registry->set('http', $http);
        }
        catch( Zend_Exception $e) {
            echo "Estamos sem conexão ao banco de dados no momento. Por favor, tente mais tarde.";
        }
    }

    /**
     * Inicializa com banco de dados Sql
     * @name _initDatabase
     * @return void
     */
    protected function _initDatabase()
    {
        //@var $resource Zend_Application_Resource_Multidb
        /*$resource = $this->getPluginResource('multidb');*/
        // inicia as conexões aos bancos de dados
        /*$resource->init();
        Zend_Registry::set('mssql', $resource->getDb('mssql'));*/
    }

    
    public function _initAcl()
    {
        $acl = new Zend_Acl();
        $acl->addRole('guest')
            ->addRole('participante', 'guest')
            ->addRole('atendente', 'guest')
            ->addRole('administrador', 'guest');
            
        $acl->addResource('main:index:index');
        $acl->addResource('main:index:logout');

        $acl->addResource('main:comofunciona:index');

        $acl->addResource('main:divulgue:index');

        $acl->addResource('main:minhaconta:index');
        $acl->addResource('main:minhaconta:criarconta');
        $acl->addResource('main:minhaconta:editarconta');
        $acl->addResource('main:minhaconta:detalhe');
        $acl->addResource('main:minhaconta:meuscupons');
        $acl->addResource('main:minhaconta:meucupom');
        $acl->addResource('main:minhaconta:printcupom');
        $acl->addResource('main:minhaconta:logout');

        $acl->addResource('main:atendente:index');
        $acl->addResource('main:atendente:validarcupom');

        $acl->addResource('main:cupons:index');
        $acl->addResource('main:cupons:categoria');
        $acl->addResource('main:cupons:detalhe');

        $acl->addResource('main:desapega:index');
        $acl->addResource('main:desapega:produtos');
        $acl->addResource('main:desapega:detalhe');
        
        $acl->addResource('main:error:error');
        $acl->addResource('main:error:denied');

        $acl->addResource('admin:index:index');
        $acl->addResource('admin:index:logout');
        $acl->addResource('admin:index:dashboard');

        $acl->addResource('admin:usuario:index');
        $acl->addResource('admin:usuario:criar');
        $acl->addResource('admin:usuario:editar');
        $acl->addResource('admin:usuario:excluir');

        $acl->addResource('admin:cupom:index');
        $acl->addResource('admin:cupom:criar');
        $acl->addResource('admin:cupom:editar');
        $acl->addResource('admin:cupom:excluir');

        $acl->addResource('admin:parceiro:index');
        $acl->addResource('admin:parceiro:criar');
        $acl->addResource('admin:parceiro:editar');
        $acl->addResource('admin:parceiro:excluir');

        $acl->addResource('admin:categoria:index');
        $acl->addResource('admin:categoria:criar');
        $acl->addResource('admin:categoria:editar');
        $acl->addResource('admin:categoria:excluir');

        $acl->addResource('admin:dashboard:index');

        $acl->allow('guest', 'main:index:index');
        $acl->allow('participante', 'main:index:logout');

        $acl->allow('guest', 'main:comofunciona:index');

        $acl->allow('guest', 'main:divulgue:index');

        $acl->allow('guest', 'main:minhaconta:index');
        $acl->allow('guest', 'main:minhaconta:criarconta');
        $acl->allow('participante', 'main:minhaconta:editarconta');
        $acl->allow('participante', 'main:minhaconta:detalhe');
        $acl->allow('participante', 'main:minhaconta:meuscupons');
        $acl->allow('participante', 'main:minhaconta:meucupom');
        $acl->allow('participante', 'main:minhaconta:printcupom');
        $acl->allow('participante', 'main:minhaconta:logout');

        $acl->allow('guest', 'main:atendente:index');
        $acl->allow('atendente', 'main:atendente:validarcupom');

        $acl->allow('guest', 'main:cupons:index');
        $acl->allow('guest', 'main:cupons:categoria');
        $acl->allow('guest', 'main:cupons:detalhe');

        $acl->allow('guest', 'main:desapega:index');
        $acl->allow('guest', 'main:desapega:produtos');
        $acl->allow('guest', 'main:desapega:detalhe');

        $acl->allow('guest', 'main:error:error');
        $acl->allow('guest', 'main:error:denied');

        $acl->allow('guest', 'admin:index:index');
        $acl->allow('guest', 'admin:index:logout');

        $acl->allow('administrador', 'admin:dashboard:index');

        $acl->allow('administrador', 'admin:usuario:index');
        $acl->allow('administrador', 'admin:usuario:criar');
        $acl->allow('administrador', 'admin:usuario:editar');
        $acl->allow('administrador', 'admin:usuario:excluir');
        
        $acl->allow('administrador', 'admin:cupom:index');
        $acl->allow('administrador', 'admin:cupom:criar');
        $acl->allow('administrador', 'admin:cupom:editar');
        $acl->allow('administrador', 'admin:cupom:excluir');

        $acl->allow('administrador', 'admin:parceiro:index');
        $acl->allow('administrador', 'admin:parceiro:criar');
        $acl->allow('administrador', 'admin:parceiro:editar');
        $acl->allow('administrador', 'admin:parceiro:excluir');

        $acl->allow('administrador', 'admin:categoria:index');
        $acl->allow('administrador', 'admin:categoria:criar');
        $acl->allow('administrador', 'admin:categoria:editar');
        $acl->allow('administrador', 'admin:categoria:excluir');
        
        Zend_Registry::set('acl', $acl);
    }
}


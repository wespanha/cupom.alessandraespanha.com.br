<?php

class Admin_Form_Login extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$email = new Zend_Form_Element_Text('email');
		$email->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control',
				 					'placeholder' => 'E-mail',
				 					'required' => ''));
		$this->addElement($email);

		$senha = new Zend_Form_Element_Password('senha');
		$senha->removeDecorator('Label')
				 ->removeDecorator('HtmlTag')
				 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control',
				 					'placeholder' => 'Senha',
				 					'required' => ''));
		$this->addElement($senha);

	}

}
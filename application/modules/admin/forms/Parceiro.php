<?php

class Admin_Form_Parceiro extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$nome = new Zend_Form_Element_Text('nome');
		$nome->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setLabel('Nome Completo')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Nome completo',
				 					'required' => ''));
		$this->addElement($nome);

		$logo = new Zend_Form_Element_File('logo');
		$logo->removeDecorator('Label')
				 ->removeDecorator('HtmlTag')
				 ->setLabel('Logo')
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Logo'))
				 ->addValidator('Count', false, array('min' =>0, 'max' => 1))
	             ->addValidator('Size', false, array('max' => '10MB'))
	             ->setDestination('images/parceiros');
		$this->addElement($logo);

	}

}
<?php

class Admin_Form_Usuario extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$nome = new Zend_Form_Element_Text('nome');
		$nome->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setLabel('Nome Completo')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Nome completo',
				 					'required' => ''));
		$this->addElement($nome);

		$email = new Zend_Form_Element_Text('email');
		$email->removeDecorator('Label')
				 ->removeDecorator('HtmlTag')
				 ->setLabel('E-mail')
				 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'E-mail',
				 					'required' => ''));
		$this->addElement($email);

		$senha = new Zend_Form_Element_Password('senha');
		$senha->removeDecorator('Label')
				 ->removeDecorator('HtmlTag')
				 ->setLabel('Senha')
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Senha'));
		$this->addElement($senha);

		$permissao = new Zend_Form_Element_Select('permissao');
		$permissao->removeDecorator('Label')
				 ->removeDecorator('HtmlTag')
				 ->setLabel('Permissão')
				 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Permissão',
				 					'required' => ''))
				 ->addMultiOptions(array('administrador' => 'Administrador'));
		$this->addElement($permissao);

		$status = new Zend_Form_Element_Select('status');
		$status->removeDecorator('Label')
				 ->removeDecorator('HtmlTag')
				 ->setLabel('Status')
				 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Status',
				 					'required' => ''))
				 ->addMultiOptions(array('s' => 'Ativo',
				 						 'n' => 'Inativo'));
		$this->addElement($status);

	}

}
<?php

class Admin_Form_Cupom extends Zend_Form 
{

	public function init() {

		$model_parceiro = new Admin_Model_Parceiro();
		$parceiros = $model_parceiro->buscarTodosParceiros();

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$idParceiro = new Zend_Form_Element_Select('idParceiro');
		$idParceiro->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setLabel('Parceiro')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Parceiro',
				 					'required' => ''));
		foreach ($parceiros as $key => $parceiro) {
			$idParceiro->addMultiOption($parceiro['id'], $parceiro['nome']);
		}

		$this->addElement($idParceiro);

		$breve_descricao = new Zend_Form_Element_Text('breve_descricao');
		$breve_descricao->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setLabel('Breve descricao')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Breve descricao',
				 					'required' => ''));
		$this->addElement($breve_descricao);

		$detalhes = new Zend_Form_Element_Text('detalhes');
		$detalhes->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setLabel('Detalhes')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Detalhes',
				 					'required' => ''));
		$this->addElement($detalhes);

		$de = new Zend_Form_Element_Text('de');
		$de->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setLabel('De')
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'De'));
		$this->addElement($de);

		$por = new Zend_Form_Element_Text('por');
		$por->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setLabel('Por')
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Por'));
		$this->addElement($por);

		$locais = new Zend_Form_Element_Text('locais');
		$locais->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setLabel('Locais')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Locais',
				 					'required' => ''));
		$this->addElement($locais);

		$quantidade = new Zend_Form_Element_Text('quantidade');
		$quantidade->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setLabel('Quantidade')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Quantidade',
				 					'required' => ''));
		$this->addElement($quantidade);

		$categoria = new Zend_Form_Element_Select('categoria');
		$categoria->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setLabel('Categoria')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Categoria',
				 					'required' => ''))
				 ->addMultiOptions(array('' => 'Selecione a categoria',
				 						 'automotivo' => 'Automotivo',
				 						 'beleza' => 'Beleza',
				 						 'bemestar' => 'Bem Estar',
				 						 'educacao' => 'Educação',
				 						 'moda' => 'Moda',
				 						 'restaurante', 'Restaurante'));
		$this->addElement($categoria);

		$imagem = new Zend_Form_Element_File('imagem');
		$imagem->removeDecorator('Label')
				 ->removeDecorator('HtmlTag')
				 ->setLabel('Imagem')
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Imagem'))
				 ->addValidator('Count', false, array('min' =>0, 'max' => 1))
	             ->addValidator('Size', false, array('max' => '10MB'))
	             ->setDestination('images/produtos');
		$this->addElement($imagem);

		$setor = new Zend_Form_Element_Select('setor');
		$setor->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setLabel('Parceiro')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Parceiro',
				 					'required' => ''))
				 ->addMultiOptions(array('pegolevo' => 'Pegô Levô',
				 						 'desapega' => 'Desapega'));

		$this->addElement($setor);

		$datainicio = new Zend_Form_Element_Text('data_inicio');
		$datainicio->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setLabel('Data de Início')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12 data',
				 					'placeholder' => 'Data de Início',
				 					'required' => ''));
		$this->addElement($datainicio);

		$datatermino = new Zend_Form_Element_Text('data_termino');
		$datatermino->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setLabel('Data de Término')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12 data',
				 					'placeholder' => 'Data de Término',
				 					'required' => ''));
		$this->addElement($datatermino);

	}

}
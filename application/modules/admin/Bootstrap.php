<?php

class Admin_Bootstrap extends Zend_Application_Module_Bootstrap
{
    /**
     * Inicializa o Namespace do model Admin
     */
    protected function _initAutoloader(){
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('Admin');
    }


}
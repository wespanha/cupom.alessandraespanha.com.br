<?php
class Admin_Model_Participante
{	
	/**
	* Armazena conexão com o Banco de Dados
	* @name db
	* @access protected
	*/
	protected $db;

	/**
	* Armazena o id do participante
	* @name id
	* @access private
	*/
	private $id;

	/**
	* Armazena o nome do participante
	* @name nome
	* @access private
	*/
	private $nome;

	/**
	* Armazena o email do participante
	* @name email
	* @access private
	*/
	private $email;

	/**
	* Armazena o celular do usuário
	* @name celular
	* @access private
	*/
	private $celular;

	/**
	* Armazena o cpf do usuário
	* @name cpf
	* @access private
	*/
	private $cpf;

	/**
	* Armazena a data de cadastro do usuário
	* @name data
	* @access private
	*/
	private $data;

	/**
	* Método construtor para instânciar a conexão com banco de dados
	* @name _construct()
	* @return void
	*/
	public function __construct() 
	{
		$this->db = Zend_Registry::get('db');
	}

	/**
	* Seta o id do participante
	* @name setId()
	* @return void
	*/
	public function setId($id) {
		$this->id = $id;
	}

	/**
	* Retorna o id do participante
	* @name getId()
	* @return int
	*/
	public function getId() {
		return $this->id;
	}

	/**
	* Seta o nome do participante
	* @name setNome()
	* @return void
	*/
	public function setNome($nome) {
		$this->nome = $nome;
	}

	/**
	* Retorna o nome do participante
	* @name getNome()
	* @return string
	*/
	public function getNome() {
		return $this->nome;
	}

	/**
	* Seta o email do participante
	* @name setEmail()
	* @return void
	*/
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	* Retorna o email do participante
	* @name getEmail()
	* @return string
	*/
	public function getEmail() {
		return $this->email;
	}

	/**
	* Seta o celular do participante
	* @name setCelular()
	* @return void
	*/
	public function setCelular($celular) {
		$this->celular = $celular;
	}

	/**
	* Retorna o celular do participante
	* @name getCelular()
	* @return string
	*/
	public function getCelular() {
		return $this->celular;
	}

	/**
	* Seta o cpf do participante
	* @name setCpf()
	* @return void
	*/
	public function setCpf($cpf) {
		$this->cpf = $cpf;
	}

	/**
	* Retorna o cpf do participante
	* @name getCpf()
	* @return string
	*/
	public function getCpf() {
		return $this->cpf;
	}

	/**
	* Seta a data de cadastro do participante
	* @name setData()
	* @return void
	*/
	public function setData($data) {
		$this->data = $data;
	}

	/**
	* Retorna a data de cadastro do participante
	* @name getData()
	* @return string
	*/
	public function getData() {
		return $this->data;
	}

}
<?php
class Admin_Model_Parceiro
{
	/**
	* Armazena conexão com o Banco de Dados
	* @name db
	* @access protected
	*/
	protected $db;

	/**
	* Id do parceiro na plataforma
	* @name id
	* @access private
	*/
	private $id;

	/**
	* Nome do parceiro
	* @name nome
	* @access private
	*/
	private $nome;

	/**
	* Logo do parceiro
	* @name logo
	* @access private
	*/
	private $logo;

	/**
	* Método construtor para instânciar a conexão com banco de dados
	* @name _construct()
	* @return void
	*/
	public function __construct() {
		$this->db = Zend_Registry::get('db');
	}

	/**
	* Busca todos os parceiros cadastrados na plataforma
	*
	* @name buscarTodosParceiros()
	* @return void
	*/
	public function buscarTodosParceiros()
	{
		$sql = $this->db->select()
						->from('parceiros');
		$resultado = $this->db->fetchAll($sql);

		foreach ($resultado as $key => $value) {
			$cupons = $this->contadorDeCuponsPorParceiros( $value['id'] );
			$resultado[$key]['qtd_cupons'] = $cupons;
		}

		return $resultado;
	}

	/**
	* Busca parceiro especifico
	*
	* @name buscarParceiro()
	* @param $id [id do parceiro]
	* @return bool
	*/
	public function buscarParceiro( $id )
	{
		$sql = $this->db->select()
						->from('parceiros')
						->where('id = ?', $id);
		$resultado = $this->db->fetchRow($sql);

		$this->id = $resultado['id'];
		$this->nome = $resultado['nome'];
		$this->logo = $resultado['logo'];
		
		return true;
	}

	/**
	* Grava os dados do parceiro na base
	*
	* @name criarParceiro()
	* @return bool
	*/
	public function criarParceiro()
	{
		$dados = [
			'nome' => $this->nome,
			'logo' => $this->logo
		];

		if( $this->db->insert('parceiros', $dados) )
			return true;

		return false;
	}

	/**
	* Atualizar informações dos parceiros na base
	*
	* @name autalizarDadosParceiro()
	* @return bool
	*/
	public function autalizarDadosParceiro()
	{
		$dados = [
			'nome' => $this->nome,
			'logo' => $this->logo
		];

		//verifica se sera necessario a atualizacao da imagem
        if( $this->getLogo() != 'vazio' ) {
        	//busca os dados do cupom cadastrados na base para poder apagar a imagem
        	$parceiro = $this->buscarParceiro( $this->getId() );
        	unlink($this->getLogo());
        } else {
        	unset($dados['logo']);
        }

        $where = $this->db->quoteInto('id = ?', $this->getId());

		if( $this->db->update('parceiros', $dados, $where) )
			return true;

		return false;
	}

	/**
	* Exclui os dados do parceiro
	*
	* @param $id [id do parceiro]
	* @name excluirParceiro()
	* @return bool
	*/
	public function excluirParceiro( $id )
	{
		$this->buscarParceiro( $id );
		if( !unlink( $this->getLogo() ) ) {
			return false;
		}

		$where = $this->db->quoteInto('id = ?', $id);

		if( $this->db->delete('parceiros', $where) )
			return true;

		return false;
	}

	/**
	* Retorna quantidade de cupons cadastrados pelo parceiro
	*
	* @param $id [id do parceiro]
	* @name contadorDeCuponsPorParceiros()
	* @return bool
	*/
	public function contadorDeCuponsPorParceiros( $id )
	{
		$sql = $this->db->select()
						->from('cupons')
						->where('idParceiro = ?',$id);
		return count($this->db->fetchAll($sql));
	}

	/**
	* Seta o id do parceiro
	* @name setId()
	* @return void
	*/
	public function setId($id) {
		$this->id = $id;
	}

	/**
	* Retorna o id do parceiro
	* @name getId()
	* @return int
	*/
	public function getId() {
		return $this->id;
	}

	/**
	* Seta o nome do parceiro
	* @name setNome()
	* @return void
	*/
	public function setNome($nome) {
		$this->nome = $nome;
	}

	/**
	* Retorna o nome do parceiro
	* @name getNome()
	* @return string
	*/
	public function getNome() {
		return $this->nome;
	}

	/**
	* Seta a logo do parceiro
	* @name setLogo()
	* @return void
	*/
	public function setLogo($logo) {
		$this->logo = $logo;
	}

	/**
	* Retorna a logo do parceiro
	* @name getLogo()
	* @return string
	*/
	public function getLogo() {
		return $this->logo;
	}
	
}
<?php
class Admin_Model_Categoria extends Admin_Model_Cupom
{

	/**
	* Id da categoria
	* @name id
	* @access private
	*/
	private $id;

	/**
	* Nome da categoria
	* @name nome
	* @access private
	*/
	private $nome;

	/**
	* Nome amigavel da categoria
	* @name slug
	* @access private
	*/
	private $slug;

	/**
	* Icone da categoria
	* @name icone
	* @access private
	*/
	private $icone;

	/**
	* Busca as categorias e quantidade de cupons para cada
	*
	* @name buscarCategorias()
	* @param $slug [nome amigavel da categoria a ser pesquisada] (Opcional)
	* @return array [$result]
	*/
	public function buscarCategorias( $slug = null )
	{
		$sql = $this->db->select()
						->from('categorias')
						->order('slug');

		//verifica se eh pra filtrar a categoria
		if( !is_null($slug) ) {

			$sql->where('slug = ?', $slug);
			$resultado = $this->db->fetchRow($sql);

			//seta os dados retornados
			$this->id = $resultado['id'];
			$this->nome = $resultado['nome'];
			$this->slug = $resultado['slug'];
			$this->icone = $resultado['icone'];

			return true;
		}
		//busca todos os registros
		$resultado = $this->db->fetchAll($sql);
		
		//percorre as categorias para consultar a quantidade de cupons dentro de cada
		foreach ($resultado as $key => $value) {
			$this->contadorCupomPorCategoria($value['slug']);

			$concat = ($this->quantidade_cupons_por_categoria == 0 || $this->quantidade_cupons_por_categoria == 1) ? ' cupom' : ' cupons';

			$resultado[$key]['contador'] = $this->quantidade_cupons_por_categoria.$concat;
		}

		return $resultado;
	}

	/**
	* Cadastra as informações da categoria na base
	*
	* @name criarCategoria()
	* @return bool
	*/
	public function criarCategoria()
	{
		$dados = [
			'nome' => $this->nome,
			'slug' => $this->slug,
			'icone' => $this->icone
		];

		if( $this->db->insert('categorias', $dados) )
			return true;

		return false;
	}

	/**
	* Atualizar informações da categoria na base
	*
	* @name autalizarDadosCategoria()
	* @return bool
	*/
	public function autalizarDadosCategoria()
	{
		$dados = [
			'nome' => $this->nome,
			'icone' => $this->icone
		];

		//verifica se sera necessario a atualizacao da imagem
        if( $this->getIcone() != 'vazio' ) {
        	//busca os dados do cupom cadastrados na base para poder apagar a imagem
        	$categoria = $this->buscarCategorias( $this->getSlug() );
        	unlink($this->getIcone());
        } else {
        	unset($dados['icone']);
        }

        $where = $this->db->quoteInto('id = ?', $this->getId());

		if( $this->db->update('categorias', $dados, $where) )
			return true;

		return false;
	}

	/**
	* Exclui as informações da categoria na base
	*
	* @param $slug [nome amigavel da categoria]
	* @name excluirCategoria()
	* @return bool
	*/
	public function excluirCategoria( $slug )
	{
		$this->buscarCategorias( $slug );
		if( !unlink( $this->getIcone() ) ) {
			return false;
		}

		$where = $this->db->quoteInto('slug = ?', $slug);

		if( $this->db->delete('categorias', $where) )
			return true;

		return false;
	}

	/**
	* Seta o valor do id da categoria
	*
	* @name setId()
	* @access public
	* @return void
	*/
	public function setId( $id ) {
		$this->id = $id;
	}

	/**
	* Retorna o valor do id da categoria
	*
	* @name getId()
	* @access public
	* @return int
	*/
	public function getId() {
		return $this->id;
	}

	/**
	* Seta o nome da categoria
	*
	* @name setNome()
	* @access public
	* @return void
	*/
	public function setNome( $nome ) {
		$this->nome = $nome;
	}

	/**
	* Retorna o nome da categoria
	*
	* @name getNome()
	* @access public
	* @return string
	*/
	public function getNome() {
		return $this->nome;
	}

	/**
	* Seta o nome amigavel da categoria
	*
	* @name setSlug()
	* @access public
	* @return void
	*/
	public function setSlug( $slug ) {
		$this->slug = $slug;
	}

	/**
	* Retorna o nome amigavel da categoria
	*
	* @name getSlug()
	* @access public
	* @return string
	*/
	public function getSlug() {
		return $this->slug;
	}

	/**
	* Seta o endereço do icone da categoria
	*
	* @name setIcone()
	* @access public
	* @return void
	*/
	public function setIcone( $icone ) {
		$this->icone = $icone;
	}

	/**
	* Retorna o endereço do icone da categoria
	*
	* @name getIcone()
	* @access public
	* @return string
	*/
	public function getIcone() {
		return $this->icone;
	}
}
?>
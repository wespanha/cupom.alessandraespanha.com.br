<?php
class Admin_Model_Util
{

	/**
	* Realiza o upload da imagem para o diretorio especificado no form
	* @name uploadImagem()
	*
	* @return string
	*/
	public function uploadImagem( $caminho )
	{
		//instacia do adaptador HTTP
		$adaptador = new Zend_File_Transfer_Adapter_Http();
		//busca o nome do arquivo
        $nome_arquivo = $adaptador->getFilename();

        //verifica se foi setado o valor
        if(!empty($nome_arquivo)) {
        	$nome_arquivo = basename($nome_arquivo);

	        //gera o token randomico
	        $token = md5(uniqid(mt_rand(), true));
	        //seta o novo nome para imagem
	        $novo_nome = new Zend_Filter_File_Rename(
	    						array('target' => $caminho . $token.$nome_arquivo, 'overwrite' => false)
	    					);
	        $adaptador->addFilter($novo_nome);

	        //atualiza arquivo
	        if (!$adaptador->receive()) {
	            return false;
	        }
	        //retorna o nome do arquivo atualizado
	        return $adaptador->getFilename();
        } else {
        	//retorna vazio para que seja validado nas situacoes que forem necessarias ou nao o campo de imagem obrigatorio
        	return 'vazio';
        }
        
	}

}
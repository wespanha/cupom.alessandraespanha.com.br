<?php

class Admin_Model_Cupom extends Main_Model_Util
{
	/**
	* Armazena conexão com o Banco de Dados
	* @name db
	* @access protected
	*/
	protected $db;

	/**
	* Numero randomico do cupom gerado pelo usuario
	* @name rand
	* @access private
	*/
	private $rand;

	/**
	* Quantidade de cupons por categoria
	* @name quantidade_cupons_por_categoria
	* @access protected
	*/
	protected $quantidade_cupons_por_categoria;

	/**
	* Id do cupom
	* @name id
	* @access private
	*/
	private $id;

	/**
	* Id do estabelecimento
	* @name idParceiro
	* @access private
	*/
	private $idParceiro;

	/**
	* Descrição curta do cupom
	* @name breve_descricao
	* @access private
	*/
	private $breve_descricao;

	/**
	* Detalhamento do produto a ser oferecido
	* @name detalhes
	* @access private
	*/
	private $detalhes;

	/**
	* Valor do produto sem desconto
	* @name de
	* @access private
	*/
	private $de;

	/**
	* Valor do produto com desconto
	* @name por
	* @access private
	*/
	private $por;

	/**
	* Local onde será possível resgatar o cupom
	* @name locais
	* @access private
	*/
	private $locais;

	/**
	* Quantidade de cupons disponiveis para o produto
	* @name quantidade
	* @access private
	*/
	private $quantidade;

	/**
	* Categoria do cupom
	* @name categoria
	* @access private
	*/
	private $categoria;

	/**
	* Imagem do produto oferecido no cupom
	* @name imagem
	* @access private
	*/
	private $imagem;

	/**
	* Setor pegolevo ou desapega
	* @name setor
	* @access private
	*/
	private $setor;

	/**
	* Data que sera disponibilizado o cupom
	* @name data_inicio
	* @access private
	*/
	private $data_inicio;

	/**
	* Data de encerramento do cupom
	* @name data_termino
	* @access private
	*/
	private $data_termino;


	/**
	* Método construtor para instânciar a conexão com banco de dados
	* @name _construct()
	* @return void
	*/
	public function __construct() {
		$this->db = Zend_Registry::get('db');
	}

	/**
	* Busca todos os cupons
	*
	* @name buscarTodosCupons()
	* @return array [$result]
	*/
	public function buscarTodosCupons()
	{
		$sql = $this->db->select()
					    ->from('cupons')
					    ->join('parceiros', 'parceiros.id = cupons.idParceiro', array('nome', 'logo'))
					    ->order(array('cupons.data_inicio DESC'));
		
		$cupons = $this->db->fetchAll($sql);

		foreach ($cupons as $key => $cupom) {
			$contador = $this->contadorDeCuponsGeradosEValidados($cupom['id']);
			$cupons[$key]['cupons_gerados'] = $contador['gerados'];
			$cupons[$key]['cupons_validados'] = $contador['validados'];
		}
		return $cupons;
	}

	/**
	* Busca um cupom especifico
	*
	* @name buscarCupom()
	* @param $id [id do cupom]
	* @return void
	*/
	public function buscarCupom( $id )
	{
		$sql = $this->db->select()
					    ->from('cupons', array('*'))
					    ->join('parceiros', 'parceiros.id = cupons.idParceiro', array('id AS idParceiro', 'nome', 'logo'))
					    ->where('cupons.id = ?', $id);
		
		$resultado = $this->db->fetchRow($sql);

		//seta os dados retornados
		$this->id = $resultado['id'];
		$this->idParceiro = $resultado['idParceiro'];
		$this->breve_descricao = $resultado['breve_descricao'];
		$this->detalhes = $resultado['detalhes'];
		$this->de = $resultado['de'];
		$this->por = $resultado['por'];
		$this->locais = $resultado['locais'];
		$this->quantidade = $resultado['quantidade'];
		$this->categoria = $resultado['categoria'];
		$this->imagem = $resultado['imagem'];
		$this->setor = $resultado['setor'];
		$this->data_inicio = implode('-', array_reverse( explode('-', $resultado['data_inicio']) ));
		$this->data_termino = implode('-', array_reverse( explode('-', $resultado['data_termino']) ));
	}

	/**
	* Cadastra as informações do cupom na base
	*
	* @name criarCupom()
	* @return bool
	*/
	public function criarCupom()
	{
		$dados = [
            'idParceiro' => $this->getIdParceiro(),
            'breve_descricao' => $this->getBreveDescricao(),
            'detalhes' => $this->getDetalhes(),
            'de' => $this->getDe(),
            'por' => $this->getPor(),
            'locais' => $this->getLocais(),
            'quantidade' => $this->getQuantidade(),
            'categoria' => $this->getCategoria(),
            'setor' => $this->getSetor(),
            'imagem' => $this->getImagem(),
            'data_inicio' => $this->getDataInicio(),
            'data_termino' => $this->getDataTermino()
        ];

        $dados['data_inicio'] = implode('-', array_reverse( explode('-', $dados['data_inicio']) ));
        $dados['data_termino'] = implode('-', array_reverse( explode('-', $dados['data_termino']) ));

		if( $this->db->insert('cupons', $dados) )
			return true;

		return false;
	}

	/**
	* Atualiza as informacoes do cupom na base
	*
	* @name atualizarDadosCupom()
	* @return bool
	*/
	public function atualizarDadosCupom()
	{
		$dados = [
            'idParceiro' => $this->getIdParceiro(),
            'breve_descricao' => $this->getBreveDescricao(),
            'detalhes' => $this->getDetalhes(),
            'de' => $this->getDe(),
            'por' => $this->getPor(),
            'locais' => $this->getLocais(),
            'quantidade' => $this->getQuantidade(),
            'categoria' => $this->getCategoria(),
            'setor' => $this->getSetor(),
            'imagem' => $this->getImagem(),
            'data_inicio' => $this->getDataInicio(),
            'data_termino' => $this->getDataTermino()
        ];

        $dados['data_inicio'] = implode('-', array_reverse( explode('-', $dados['data_inicio']) ));
        $dados['data_termino'] = implode('-', array_reverse( explode('-', $dados['data_termino']) ));

        //verifica se sera necessario a atualizacao da imagem
        if( $this->getImagem() != 'vazio' ) {
        	//busca os dados do cupom cadastrados na base para poder apagar a imagem
        	$cupom = $this->buscarCupom( $this->getId() );
        	unlink($this->getImagem());
        } else {
        	unset($dados['imagem']);
        }

        $where = $this->db->quoteInto('id = ?', $this->getId());

		if( $this->db->update('cupons', $dados, $where) )
			return true;

		return false;
	}

	/**
	* Exclui os dados do cupom da base
	*
	* @name excluirCupom()
	* @param $id [id do cupom]
	* @return bool
	*/
	public function excluirCupom( $id )
	{
		$this->buscarCupom( $id );
		if( !unlink( $this->getImagem() ) ) {
			return false;
		}

		$where = $this->db->quoteInto('id = ?', $id);

		if( $this->db->delete('cupons', $where) )
			return true;

		return false;
	}

	/**
	* Retorna a quantidade de cupons foram gerados e validados
	*
	* @name contadorDeCuponsGeradosEValidados()
	* @param $id [id do cupom]
	* @return array
	*/
	public function contadorDeCuponsGeradosEValidados( $id )
	{
		$resultado = array();
		$sql = $this->db->select()
						->from('cupons_participantes')
						->where('idCupom = ?', $id);
		$resultado['gerados'] = count($this->db->fetchAll($sql));

		$sql = $this->db->select()
						->from('cupons_participantes')
						->where('idCupom = ?', $id)
						->where('validado = "s"');
		$resultado['validados'] = count($this->db->fetchAll($sql));

		return $resultado;
	}

	/**
	* Verifica a quantidade de cupons por categoria
	*
	* @name contadorCupomPorCategoria()
	* @param $categoria [nome amigavel da categoria a ser filtrada] (Obrigatório)
	* @return array
	*/
	public function contadorCupomPorCategoria( $categoria )
	{
		$data = Date('Y-m-d');
		$sql = $this->db->select()
						->from('cupons', array('COUNT(*) AS quantidade'))
						->where('data_inicio <= ?', $data)
					    ->where('data_termino >= ?', $data)
						->where('cupons.categoria = ?', $categoria)
						->where('cupons.setor = "pegolevo"');

		$resultado = $this->db->fetchRow($sql);

		$this->quantidade_cupons_por_categoria = $resultado['quantidade'];
	}

	/**
	* Seta o valor do id do cupom
	*
	* @name setId()
	* @access public
	* @return void
	*/
	public function setId( $id ) {
		$this->id = $id;
	}

	/**
	* Retorna o valor do id do cupom
	*
	* @name getId()
	* @access public
	* @return int
	*/
	public function getId() {
		return $this->id;
	}

	/**
	* Seta o valor do id do parceiro
	*
	* @name setIdParceiro()
	* @access public
	* @return void
	*/
	public function setIdParceiro( $idParceiro ) {
		$this->idParceiro = $idParceiro;
	}

	/**
	* Retorna o valor do id do parceiro
	*
	* @name getIdParceiro()
	* @access public
	* @return int
	*/
	public function getIdParceiro() {
		return $this->idParceiro;
	}

	/**
	* Seta a descricao curta do cupom
	*
	* @name setBreveDescricao()
	* @access public
	* @return void
	*/
	public function setBreveDescricao( $breve_descricao ) {
		$this->breve_descricao = $breve_descricao;
	}

	/**
	* Retorna a descricao curta do cupom
	*
	* @name getBreveDescricao()
	* @access public
	* @return string
	*/
	public function getBreveDescricao() {
		return $this->breve_descricao;
	}

	/**
	* Seta o detalhe completo do produto
	*
	* @name setDetalhes()
	* @access public
	* @return void
	*/
	public function setDetalhes( $detalhes ) {
		$this->detalhes = $detalhes;
	}

	/**
	* Retorna o detalhe completo do produto
	*
	* @name getDetalhes()
	* @access public
	* @return string
	*/
	public function getDetalhes() {
		return $this->detalhes;
	}

	/**
	* Seta o valor do produto sem desconto
	*
	* @name setDe()
	* @access public
	* @return void
	*/
	public function setDe( $de ) {
		$this->de = $de;
	}

	/**
	* Retorna o valor do produto sem desconto
	*
	* @name getDe()
	* @access public
	* @return string
	*/
	public function getDe() {
		return $this->de;
	}

	/**
	* Seta o valor do produto com desconto
	*
	* @name setPor()
	* @access public
	* @return void
	*/
	public function setPor( $por ) {
		$this->por = $por;
	}

	/**
	* Retorna o valor do produto com desconto
	*
	* @name getPor()
	* @access public
	* @return string
	*/
	public function getPor() {
		return $this->por;
	}

	/**
	* Seta os locais que o produto estara disponivel
	*
	* @name setLocais()
	* @access public
	* @return void
	*/
	public function setLocais( $locais ) {
		$this->locais = $locais;
	}

	/**
	* Retorna os locais em que o produto estara disponivel
	*
	* @name getLocais()
	* @access public
	* @return string
	*/
	public function getLocais() {
		return $this->locais;
	}

	/**
	* Seta a quantidade de cupons disponiveis para o produto
	*
	* @name setQuantidade()
	* @access public
	* @return void
	*/
	public function setQuantidade( $quantidade ) {
		$this->quantidade = $quantidade;
	}

	/**
	* Retorna a quantidade de cupons disponiveis para o produto
	*
	* @name getQuantidade()
	* @access public
	* @return string
	*/
	public function getQuantidade() {
		return $this->quantidade;
	}

	/**
	* Seta a categoria do cupom
	*
	* @name setCategoria()
	* @access public
	* @return void
	*/
	public function setCategoria( $categoria ) {
		$this->categoria = $categoria;
	}

	/**
	* Retorna a categoria do cupom
	*
	* @name getCategoria()
	* @access public
	* @return string
	*/
	public function getCategoria() {
		return $this->categoria;
	}

	/**
	* Seta a imagem do produto
	*
	* @name setImagem()
	* @access public
	* @return void
	*/
	public function setImagem( $imagem ) {
		$this->imagem = $imagem;
	}

	/**
	* Retorna a imagem do produto
	*
	* @name getImagem()
	* @access public
	* @return string
	*/
	public function getImagem() {
		return $this->imagem;
	}

	/**
	* Seta o setor do cupom [pegolevo, desapega]
	*
	* @name setSetor()
	* @access public
	* @return void
	*/
	public function setSetor( $setor ) {
		$this->setor = $setor;
	}

	/**
	* Retorna o setor do cupom [pegolevo, desapega]
	*
	* @name getSetor()
	* @access public
	* @return string
	*/
	public function getSetor() {
		return $this->setor;
	}

	/**
	* Seta a data de inicio do cupom
	*
	* @name setDataInicio()
	* @access public
	* @return void
	*/
	public function setDataInicio( $dataInicio ) {
		$this->data_inicio = $dataInicio;
	}

	/**
	* Retorna a data de inicio do cupom
	*
	* @name getDataInicio()
	* @access public
	* @return string
	*/
	public function getDataInicio() {
		return $this->data_inicio;
	}

	/**
	* Seta a data de termino do cupom
	*
	* @name setDataTermino()
	* @access public
	* @return void
	*/
	public function setDataTermino( $dataTermino ) {
		$this->data_termino = $dataTermino;
	}

	/**
	* Retorna a data de termino do cupom
	*
	* @name getDataTermino()
	* @access public
	* @return string
	*/
	public function getDataTermino() {
		return $this->data_termino;
	}

}
<?php

class Admin_Model_Usuario
{

	/**
	* Armazena conexão com o Banco de Dados
	* @name db
	* @access private
	*/
	private $db;

	/**
	* Armazena o id do usuário
	* @name id
	* @access private
	*/
	private $id;
	
	/**
	* Armazena o nome do usuário
	* @nome nome
	* @access private
	*/
	private $nome;

	/**
	* Armazena o email do usuário
	* @name email
	* @access private
	*/
	private $email;

	/**
	* Armazena a senha de acesso do usuário
	* @name senha
	* @access private
	*/
	private $senha;

	/**
	* Armazena a permissão do usuário no painel
	* @name permissao
	* @access private
	*/
	private $permissao;

	/**
	* Armazena o status do usuário no painel
	* @name status
	* @access private
	*/
	private $status;

	/**
	* Método construtor para instânciar a conexão com banco de dados
	* @name _construct()
	* @return void
	*/
	public function __construct() 
	{
		$this->db = Zend_Registry::get('db');
	}

	/**
	* Método para buscar um usuário especifico
	* @name buscarUsuarioPorIdOuEmail()
	* @param $id [id do usuário]
	* @param $email [email do usuario]
	* @return bool
	*/
	public function buscarUsuarioPorIdOuEmail( $id = null, $email = null )
	{
		$sql = $this->db->select()
					    ->from('admin_usuario', array('id','nome','email','permissao','status'));

		if( !is_null($id) )
			$sql = $sql->where('id = ?', $id);

		if( !is_null($email) )
			$sql = $sql->where('email = ?', $email);
		
		$result = $this->db->fetchRow($sql);

		if( $result ) {
			$this->id = $result['id'];
			$this->nome = $result['nome'];
			$this->email = $result['email'];
			$this->permissao = $result['permissao'];
			$this->status = $result['status'];

			return true;
		}

		return false;
	}

	/**
	* Método para buscar todos os usuários
	* @name buscarTodosOsUsuarios()
	* @param $status [filtrar por usuários ativos ou desativados (y, n)]
	* @return array
	*/
	public function buscarTodosOsUsuarios( $status = null ) 
	{
		$sql = $this->db->select()
					    ->from('admin_usuario', array('id','nome','email','permissao','status'))
					    ->order('nome');

		if( !is_null($status) )
			$sql = $sql->where('status = ?', $status);
		
		$result = $this->db->fetchAll($sql);
		return ( $result ) ? $result : false;

	}

	/**
	* Método para criar usuário
	* @name criarUsuario()
	* @return bool
	*/
	public function criarUsuario() 
	{
		if( !$this->buscarUsuarioPorIdOuEmail(null, $this->email) && $this->senha != '' ) {
			$dados = [
				'nome' => $this->nome,
				'email' => $this->email,
				'senha' => sha1($this->senha),
				'permissao' => $this->permissao,
				'status' => $this->status
			];

			if( $this->db->insert('admin_usuario', $dados) )
				return true;
		}

		return false;
	}

	/**
	* Método para deletar o usuário
	* @name excluirUsuario()
	* @param $id [id do usuario a ser excluido]
	* @return bool
	*/
	public function excluirUsuario( $id ) 
	{
		$where = $this->db->quoteInto('id = ?', $id);

		if( $this->db->delete('admin_usuario', $where) )
			return true;

		return false;
	}

	/**
	* Método para atualizar os registro do usuário
	* @name atualizarDadosUsuario()
	* @return bool
	*/
	public function atualizarDadosUsuario() 
	{
		//if( $this->find(null, $this->email) ) {
			$data = [
				'nome' => $this->nome,
				'email' => $this->email,
				'permissao' => $this->permissao,
				'status' => $this->status
			];
			
			if( $this->senha != '' )
				$data['senha'] = sha1($this->senha);

			$where = $this->db->quoteInto('id = ?', $this->id);

			if( $this->db->update('admin_usuario', $data, $where) )
				return true;
		//}

		return false;

	}

	/**
	* Seta o id do usuário
	* @name setId()
	* @return void
	*/
	public function setId($id) {
		$this->id = $id;
	}

	/**
	* Retorna o id do usuário
	* @name getId()
	* @return int
	*/
	public function getId() {
		return $this->id;
	}

	/**
	* Seta o nome do usuário
	* @name setNome()
	* @return void
	*/
	public function setNome($nome) {
		$this->nome = $nome;
	}

	/**
	* Retorna o nome do usuário
	* @name getNome()
	* @return string
	*/
	public function getNome() {
		return $this->nome;
	}

	/**
	* Seta o email do usuário
	* @name setEmail()
	* @return void
	*/
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	* Retorna o email do usuário
	* @name getEmail()
	* @return string
	*/
	public function getEmail() {
		return $this->email;
	}

	/**
	* Seta a senha do usuário
	* @name setSenha()
	* @return void
	*/
	public function setSenha($senha) {
		$this->senha = $senha;
	}

	/**
	* Retorna a senha do usuário
	* @name getSenha()
	* @return string
	*/
	public function getSenha() {
		return $this->senha;
	}

	/**
	* Seta o status do usuário
	* @name setStatus()
	* @return void
	*/
	public function setStatus($status) {
		$this->status = $status;
	}

	/**
	* Retorna o status do usuário
	* @name getStatus()
	* @return string
	*/
	public function getStatus() {
		return $this->status;
	}

	/**
	* Seta a permissão do usuário
	* @name setPermissao()
	* @return void
	*/
	public function setPermissao($permissao) {
		$this->permissao = $permissao;
	}

	/**
	* Retorna a permissão do usuário
	* @name getPermissao()
	* @return string
	*/
	public function getPermissao() {
		return $this->permissao;
	}

}
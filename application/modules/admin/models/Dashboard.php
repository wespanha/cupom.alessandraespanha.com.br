<?php
class Admin_Model_Dashboard
{
	/**
	* Armazena conexão com o Banco de Dados
	* @name db
	* @access protected
	*/
	protected $db;

	/**
	* Método construtor para instânciar a conexão com banco de dados
	* @name _construct()
	* @return void
	*/
	public function __construct() {
		$this->db = Zend_Registry::get('db');
	}

	/**
	* Contador de participantes cadastrados no sistema
	*
	* @name contadorParticipantes()
	* @return int
	*/
	public function contadorParticipantes() 
	{
		$sql = $this->db->select()
						->from('participantes');
		return count($this->db->fetchAll($sql));
	}

	/**
	* Contador de parceiros cadastrados no sistema
	*
	* @name contadorParceiros()
	* @return int
	*/
	public function contadorParceiros()
	{
		$sql = $this->db->select()
						->from('parceiros');
		return count($this->db->fetchAll($sql));
	}

	/**
	* Contador de cupons cadastrados no sistema
	*
	* @name contadorCupons()
	* @return int
	*/
	public function contadorCupons()
	{
		$sql = $this->db->select()
						->from('cupons');
		return count($this->db->fetchAll($sql));
	}

	/**
	* Contador de cupons ativos para os participantes
	*
	* @name contadorCuponsAtivos()
	* @return int
	*/
	public function contadorCuponsAtivos()
	{
		$data = Date('Y-m-d');
		$sql = $this->db->select()
						->from('cupons')
						->where('data_inicio <= ?', $data)
					    ->where('data_termino >= ?', $data);
		return count($this->db->fetchAll($sql));
	}

	/**
	* Contador de cupons gerados pelos participantes
	*
	* @name contadorCuponsResgatados()
	* @return int
	*/
	public function contadorCuponsResgatados()
	{
		$sql = $this->db->select()
						->from('cupons_participantes');
		return count($this->db->fetchAll($sql));
	}

	/**
	* Contador de cupons que foram validados pelos atendentes
	*
	* @name contadorCuponsValidados()
	* @return int
	*/
	public function contadorCuponsValidados()
	{
		$sql = $this->db->select()
						->from('cupons_participantes')
						->where('validado = "s"');
		return count($this->db->fetchAll($sql));
	}
	
}
<?php

class Admin_ParceiroController extends Zend_Controller_Action
{
    private $form_parceiro;

    private $model_util;

    private $model_parceiro;

    public function init() 
    {
        //form parceiro
	    $this->form_parceiro = new Admin_Form_Parceiro();
        //model util
        $this->model_util = new Admin_Model_Util();
        //model parceiro
        $this->model_parceiro = new Admin_Model_Parceiro();
    }

    public function indexAction() 
    {
    	//listagem de categorias
        $this->view->assign('parceiros', $this->model_parceiro->buscarTodosParceiros());

        if( $this->getRequest()->getParam('mensagem') ) {
            $this->view->assign('mensagem', $this->getRequest()->getParam('mensagem'));
            $this->view->assign('status_alerta', $this->getRequest()->getParam('status_alerta'));
        }
    }

    public function criarAction() 
    {
    	if( $this->getRequest()->isPost() ) {
            if( $this->form_parceiro->isValid($this->getRequest()->getPost()) ) {
                //faz o upload da imagem e altera o nome
                $imagem = $this->model_util->uploadImagem('images/parceiros/');
                //verifica se foi realizado o upload
                if( $imagem && $imagem != 'vazio' ) {
                    //busca os dados do formulario
                    $dados = $this->form_parceiro->getValues();
                    $this->model_parceiro->setNome($dados['nome']);  
                    $this->model_parceiro->setLogo($imagem);
                    //grava os dados na base
                    if( $this->model_parceiro->criarParceiro() ) {
                        $mensagem = $this->mensagem(
                            'Parceiro cadastrado com sucesso!', 'sucesso'
                        );
                    } else {
                        $mensagem = $this->mensagem(
                            'Não foi possível cadastrar o parceiro. Favor tente novamente mais tarde!', 'erro'
                        );
                    }
                } else {
                    $mensagem = $this->mensagem(
                            'Não foi possível cadastrar a imagem. Favor tente novamente mais tarde!', 'erro'
                        );
                }
                //direciona para index e assina a mensagem de retorno
                $this->forward('index', 'parceiro', 'admin', $mensagem);
            }
        }
        //assina para view o formulario
        $this->view->assign('form', $this->form_parceiro);
    }

    public function editarAction() 
    {
    	//verifica se foi passad o parametro para setar os dados no formulario
        if( $id = $this->getRequest()->getParam('id') ) {
            //busca categoria
            $this->model_parceiro->buscarParceiro( $id );
            $formata_dados = [
                'id' => $this->model_parceiro->getId(),
                'nome' => $this->model_parceiro->getNome()
            ];
            //popula os dados no formulario
            $this->form_parceiro->populate($formata_dados);
        }

        if( $this->getRequest()->isPost() ) {
            if( $this->form_parceiro->isValid($this->getRequest()->getPost()) ) {
                //verifica se foi realizado o upload
                if( $imagem = $this->model_util->uploadImagem('images/parceiros/') ) {
                    //busca os dados do formulario
                    $dados = $this->form_parceiro->getValues();
                    $this->model_parceiro->setNome($dados['nome']);
                    $this->model_parceiro->setLogo($imagem);
                    //grava os dados na base
                    if( $this->model_parceiro->autalizarDadosParceiro() ) {
                        $mensagem = $this->mensagem(
                            'Parceiro editado com sucesso!', 'sucesso'
                        );
                    } else {
                        $mensagem = $this->mensagem(
                            'Não foi possível editar o parceiro. Favor tente novamente mais tarde!', 'erro'
                        );
                    }
                } else {
                    $mensagem = $this->mensagem(
                            'Não foi possível alterar a imagem. Favor tente novamente mais tarde!', 'erro'
                        );
                }
                //direciona para index e assina a mensagem de retorno
                $this->forward('index', 'parceiro', 'admin', $mensagem);
            }
        }

        //assina para view o formulario
        $this->view->assign('form', $this->form_parceiro);
    }

    public function excluirAction() 
    {
    	if( $id = $this->getRequest()->getParam('id') ) {
            if( $this->model_parceiro->excluirParceiro($id) ) {
                $mensagem = $this->mensagem( 'Parceiro excluido com sucesso!', 'sucesso' );
            } else {
                $mensagem = $this->mensagem( 'Erro ao excluir parceiro, tente novamente!', 'erro' );
            }
            $this->forward('index', 'parceiro', 'admin', $mensagem);
        }
    }

    public function mensagem( $mensagem, $status = null ) 
    {
    	switch ($status) {
    		case 'sucesso':
    			$retorno = [
    				'status_alerta' => 'alert-success',
    				'mensagem' => $mensagem
    			];
    			break;
    		case 'erro':
    			$retorno = [
    				'status_alerta' => 'alert-error',
    				'mensagem' => $mensagem
    			];
    			break;
    		default:
    			$retorno = [
    				'status_alerta' => 'alert-info',
    				'mensagem' => $mensagem
    			];
    			break;
    	}
    	return $retorno;
    }

}
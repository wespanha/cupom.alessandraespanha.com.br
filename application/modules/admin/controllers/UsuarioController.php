<?php

class Admin_UsuarioController extends Zend_Controller_Action
{

	/**
	* Armazena o formulario do usuario
	*
	* @access private
	*/
	private $form_usuario;

	/**
	* Armazena o model do usuario
	*
	* @access private
	*/
	private $model_usuario;

	/**
	* Armazena dados do usuario logado no sistema
	*
	* @access private
	*/
	private $dados_autenticacao;

    public function init() 
    {
    	//FORMULARIO para os dados do usuario
    	$this->form_usuario = new Admin_Form_Usuario();
    	//MODEL para manipular informações do usuario
    	$this->model_usuario = new Admin_Model_Usuario();
    	//assina para view os dados do usuario autenticado
    	$this->dados_autenticacao = Zend_Auth::getInstance()->getStorage()->read();
    	$this->view->assign('dados_autenticacao', $this->dados_autenticacao);
    }

    public function indexAction() 
    {
    	$usuarios = $this->model_usuario->buscarTodosOsUsuarios();
    	$this->view->assign('usuarios', $usuarios);

		if( $this->getRequest()->getParam('mensagem') ) {
			$this->view->assign('mensagem', $this->getRequest()->getParam('mensagem'));
			$this->view->assign('status_alerta', $this->getRequest()->getParam('status_alerta'));
		}
    }

    public function criarAction() 
    {
    	if( $this->getRequest()->isPost() ) {
    		if( $this->form_usuario->isValid($this->getRequest()->getPost()) ) {
    			$dados = $this->form_usuario->getValues();
                //seta os dados do formulario para o model
    			$this->model_usuario->setNome($dados['nome']);
    			$this->model_usuario->setEmail($dados['email']);
    			$this->model_usuario->setSenha($dados['senha']);
    			$this->model_usuario->setPermissao($dados['permissao']);
    			$this->model_usuario->setStatus($dados['status']);

    			if( $this->model_usuario->criarUsuario() ) {
    				$mensagem = $this->mensagem('Usuário criado com sucesso!', 'sucesso');
    			} else {
    				$mensagem = $this->mensagem('Não foi possível criar o usuário, tente novamente!', 'erro');
    			}
    			//direciona para tela de listagem e exibe a mensagem de retorno
    			$this->forward('index', 'usuario', 'admin', $mensagem);
    		}
    	}
    	$this->view->assign('form', $this->form_usuario);
    }

    public function editarAction() 
    {
    	if( $id = $this->getRequest()->getParam('id') ) {
    		$dados_usuario = $this->model_usuario->buscarUsuarioPorIdOuEmail( $id );
            //formata os dados para popular no formulario
    		$dados_formatado = [
    			'id' => $this->model_usuario->getId(),
    			'nome' => $this->model_usuario->getNome(),
    			'email' => $this->model_usuario->getEmail(),
    			'permissao' => $this->model_usuario->getPermissao(),
    			'status' => $this->model_usuario->getStatus()
    		];
    		$this->form_usuario->populate($dados_formatado);
    	}

    	if( $this->getRequest()->isPost() ) {
    		if( $this->form_usuario->isValid($this->getRequest()->getPost()) ) {
    			$dados = $this->form_usuario->getValues();
                //seta os dados do formulario para o model
    			$this->model_usuario->setNome($dados['nome']);
    			$this->model_usuario->setEmail($dados['email']);
    			$this->model_usuario->setSenha($dados['senha']);
    			$this->model_usuario->setPermissao($dados['permissao']);
    			$this->model_usuario->setStatus($dados['status']);

    			if( $this->model_usuario->atualizarDadosUsuario() ) {
    				$mensagem = $this->mensagem('Usuário editado com sucesso!', 'sucesso');
    			} else {
    				$mensagem = $this->mensagem('Não foi possível editar o usuário, tente novamente!', 'erro');
    			}
    			//direciona para tela de listagem e exibe a mensagem de retorno
    			$this->forward('index', 'usuario', 'admin', $mensagem);
    		}
    	}

    	$this->view->assign('form', $this->form_usuario);
    }

    public function excluirAction() 
    {
    	if( $id = $this->getRequest()->getParam('id') ) {
    		if( $this->model_usuario->excluirUsuario( $id ) ) {
    			$mensagem = $this->mensagem('Usuário excluído com sucesso!', 'sucesso');
    		} else {
				$mensagem = $this->mensagem('Não foi possível excluir o usuário, tente novamente!', 'erro');
    		}
    		$this->forward('index', 'usuario', 'admin', $mensagem);
    	}
    }

    public function mensagem( $mensagem, $status = null ) 
    {
    	switch ($status) {
    		case 'sucesso':
    			$retorno = [
    				'status_alerta' => 'alert-success',
    				'mensagem' => $mensagem
    			];
    			break;
    		case 'erro':
    			$retorno = [
    				'status_alerta' => 'alert-error',
    				'mensagem' => $mensagem
    			];
    			break;
    		default:
    			$retorno = [
    				'status_alerta' => 'alert-info',
    				'mensagem' => $mensagem
    			];
    			break;
    	}
    	return $retorno;
    }

}
<?php

class Admin_IndexController extends Zend_Controller_Action
{

	private $form_login;

	private $model_auth;

    public function init() 
    {
    	$this->form_login = new Admin_Form_Login();
    }

    public function indexAction() 
    {
    	$this->_helper->layout->disableLayout();

		//verifica se foi submetido o formulario
		if( $this->getRequest()->isPost() ) {
			$form_data = $this->getRequest()->getPost();
			//se o formulario foi preenchido corretamente
			if( $this->form_login->isValid($form_data) ){
				//verifica se usuário e senha estão corretos
				$auth = Plugins_Auth_AuthSetupAdmin::login($form_data['email'], $form_data['senha']);
				if( $auth ) {
					$this->redirect('/admin/dashboard');
				} else {
					$this->view->assign('alert_msg', 'Ops! Usuário ou senha inválidos.');
					$this->view->assign('type_alert_msg', 'error');
				}
			}
		}
		//assina para view o formulário de login
		$this->view->assign('form_login', $this->form_login);
    }

    public function logoutAction() 
    {
    	$auth = Zend_Auth::getInstance();
    	$auth->clearIdentity();

    	$this->redirect('/admin');

    }

}






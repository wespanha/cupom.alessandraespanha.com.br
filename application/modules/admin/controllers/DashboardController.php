<?php

class Admin_DashboardController extends Zend_Controller_Action
{
	private $model_dashboard;

    public function init() 
    {
    	$this->model_dashboard = new Admin_Model_Dashboard();
    }

    public function indexAction() 
    {
    	$this->view->assign('contador_participantes', $this->model_dashboard->contadorParticipantes());
    	$this->view->assign('contador_parceiros', $this->model_dashboard->contadorParceiros());
    	$this->view->assign('contador_cupons', $this->model_dashboard->contadorCupons());
    	$this->view->assign('contador_cuponsativos', $this->model_dashboard->contadorCuponsAtivos());
    	$this->view->assign('contador_cuponsresgatados', $this->model_dashboard->contadorCuponsResgatados());
    	$this->view->assign('contador_cuponsvalidados', $this->model_dashboard->contadorCuponsValidados());
    }

}
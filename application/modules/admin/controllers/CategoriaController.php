<?php

class Admin_CategoriaController extends Zend_Controller_Action
{	
	/**
	* Armazena conexão com o Banco de Dados
	* @name db
	* @access private
	*/
	private $db;

	private $model_categoria;

	private $form_categoria;

    private $model_util;

    public function init()
    {
    	//model categoria
		$this->model_categoria = new Admin_Model_Categoria();
		//form categoria
		$this->form_categoria = new Admin_Form_Categoria();
        //model util
        $this->model_util = new Admin_Model_Util();
		//instacia da conexao com o banco de dados
		$this->db = Zend_Registry::get('db');
    }

    public function indexAction() 
    {
		//listagem de categorias
		$this->view->assign('categorias', $this->model_categoria->buscarCategorias());

		if( $this->getRequest()->getParam('mensagem') ) {
			$this->view->assign('mensagem', $this->getRequest()->getParam('mensagem'));
			$this->view->assign('status_alerta', $this->getRequest()->getParam('status_alerta'));
		}
    }

    public function criarAction() 
    {
        if( $this->getRequest()->isPost() ) {
            if( $this->form_categoria->isValid($this->getRequest()->getPost()) ) {
                //faz o upload da imagem e altera o nome
                $imagem = $this->model_util->uploadImagem('images/categorias/');
                //verifica se foi realizado o upload
                if( $imagem && $imagem != 'vazio' ) {
                    //busca os dados do formulario
                    $dados = $this->form_categoria->getValues();
                    $this->model_categoria->setNome($dados['nome']);    
                    $this->model_categoria->setSlug($dados['slug']);    
                    $this->model_categoria->setIcone($imagem);
                    //grava os dados na base
                    if( $this->model_categoria->criarCategoria() ) {
                        $mensagem = $this->mensagem(
                            'Categoria cadastrada com sucesso!', 'sucesso'
                        );
                    } else {
                        $mensagem = $this->mensagem(
                            'Não foi possível cadastrar a categoria. Favor tente novamente mais tarde!', 'erro'
                        );
                    }
                } else {
                    $mensagem = $this->mensagem(
                            'Não foi possível cadastrar a imagem. Favor tente novamente mais tarde!', 'erro'
                        );
                }
                //direciona para index e assina a mensagem de retorno
                $this->forward('index', 'categoria', 'admin', $mensagem);
            }
        }
    	//assina para view o formulario
    	$this->view->assign('form', $this->form_categoria);
    }

    public function editarAction()
    {	
    	//verifica se foi passad o parametro para setar os dados no formulario
    	if( $slug = $this->getRequest()->getParam('slug') ) {
    		//busca categoria
			$this->model_categoria->buscarCategorias( $slug );
			$formata_dados = [
				'id' => $this->model_categoria->getId(),
				'nome' => $this->model_categoria->getNome(),
				'slug' => $this->model_categoria->getSlug()
			];
			//popula os dados no formulario
			$this->form_categoria->populate($formata_dados);
    	}

    	if( $this->getRequest()->isPost() ) {
    		if( $this->form_categoria->isValid($this->getRequest()->getPost()) ) {
                //verifica se foi realizado o upload
                if( $imagem = $this->model_util->uploadImagem('images/categorias/') ) {
                    //busca os dados do formulario
                    $dados = $this->form_categoria->getValues();
                    $this->model_categoria->setNome($dados['nome']);    
                    $this->model_categoria->setSlug($dados['slug']);    
                    $this->model_categoria->setIcone($imagem);
                    //grava os dados na base
                    if( $this->model_categoria->autalizarDadosCategoria() ) {
                        $mensagem = $this->mensagem(
                            'Categoria editada com sucesso!', 'sucesso'
                        );
                    } else {
                        $mensagem = $this->mensagem(
                            'Não foi possível editar a categoria. Favor tente novamente mais tarde!', 'erro'
                        );
                    }
                } else {
                    $mensagem = $this->mensagem(
                            'Não foi possível alterar a imagem. Favor tente novamente mais tarde!', 'erro'
                        );
                }
                //direciona para index e assina a mensagem de retorno
                $this->forward('index', 'categoria', 'admin', $mensagem);
            }
    	}

    	//assina para view o formulario
    	$this->view->assign('form', $this->form_categoria);
    }

    public function excluirAction()
    {
    	if( $slug = $this->getRequest()->getParam('slug') ) {
			if( $this->model_categoria->excluirCategoria($slug) ) {
				$mensagem = $this->mensagem( 'Categoria excluida com sucesso!', 'sucesso' );
			} else {
				$mensagem = $this->mensagem( 'Erro ao excluir categoria, tente novamente!', 'erro' );
			}
			$this->forward('index', 'categoria', 'admin', $mensagem);
		}
    }

    public function mensagem( $mensagem, $status = null ) 
    {
    	switch ($status) {
    		case 'sucesso':
    			$retorno = [
    				'status_alerta' => 'alert-success',
    				'mensagem' => $mensagem
    			];
    			break;
    		case 'erro':
    			$retorno = [
    				'status_alerta' => 'alert-error',
    				'mensagem' => $mensagem
    			];
    			break;
    		default:
    			$retorno = [
    				'status_alerta' => 'alert-info',
    				'mensagem' => $mensagem
    			];
    			break;
    	}
    	return $retorno;
    }

}
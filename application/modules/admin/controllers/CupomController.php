<?php

class Admin_CupomController extends Zend_Controller_Action
{
    private $model_cupom;

    private $form_cupom;

    private $model_util;

    public function init() 
    {
        //model cupom
        $this->model_cupom = new Admin_Model_Cupom();
        //model util
        $this->model_util = new Admin_Model_Util();
        //form cupom
        $this->form_cupom = new Admin_Form_Cupom();
    }

    public function indexAction() 
    {
        if( $this->getRequest()->getParam('mensagem') ) {
            $this->view->assign('mensagem', $this->getRequest()->getParam('mensagem'));
            $this->view->assign('status_alerta', $this->getRequest()->getParam('status_alerta'));
        }

        $this->view->assign('cupons', $this->model_cupom->buscarTodosCupons());
    }

    public function criarAction()
    {
        if( $this->getRequest()->isPost() ) {
            if( $this->form_cupom->isValid( $this->getRequest()->getPost() ) ) {
                //faz o upload da imagem
                $nome_imagem = $this->model_util->uploadImagem('images/produtos/');
                //verifica se foi realizado o upload da imagem
                if( $nome_imagem && $nome_imagem != 'vazio' ) {
                    //busca os dados do formulario
                    $dados = $this->form_cupom->getValues();
                    //seta os dados do formulario para o model
                    $this->model_cupom->setIdParceiro($dados['idParceiro']);
                    $this->model_cupom->setBreveDescricao($dados['breve_descricao']);
                    $this->model_cupom->setDetalhes($dados['detalhes']);
                    $this->model_cupom->setDe($dados['de']);
                    $this->model_cupom->setPor($dados['por']);
                    $this->model_cupom->setLocais($dados['locais']);
                    $this->model_cupom->setQuantidade($dados['quantidade']);
                    $this->model_cupom->setCategoria($dados['categoria']);
                    $this->model_cupom->setImagem($nome_imagem);
                    $this->model_cupom->setSetor($dados['setor']);
                    $this->model_cupom->setDataInicio($dados['data_inicio']);
                    $this->model_cupom->setDataTermino($dados['data_termino']);

                    //cadastra as informacoes do cupom na base
                    if( $this->model_cupom->criarCupom() ) {
                        $mensagem = $this->mensagem(
                                'Cupom cadastrado com sucesso!', 'sucesso'
                            );
                    } else {
                        $mensagem = $this->mensagem(
                                'Não foi possível cadastrar o cupom. Tente novamente mais tarde!', 'erro'
                            );
                    }
                    
                } else {
                    $mensagem = $this->mensagem(
                            'Não foi possível cadastrar a imagem do cupom. Tente novamente mais tarde!', 'erro'
                        );
                }
                //direciona para index e assina a mensagem de retorno
                $this->forward('index', 'cupom', 'admin', $mensagem);
            }
        }
        //assina para view o formulario
        $this->view->assign('form', $this->form_cupom);
    }

    public function editarAction()
    {
        if( $id = $this->getRequest()->getParam('id') ) {
            $cupom = $this->model_cupom->buscarCupom( $id );
            $formata_dados = [
                'id' => $this->model_cupom->getId(),
                'idParceiro' => $this->model_cupom->getIdParceiro(),
                'breve_descricao' => $this->model_cupom->getBreveDescricao(),
                'detalhes' => $this->model_cupom->getDetalhes(),
                'de' => $this->model_cupom->getDe(),
                'por' => $this->model_cupom->getPor(),
                'locais' => $this->model_cupom->getLocais(),
                'quantidade' => $this->model_cupom->getQuantidade(),
                'categoria' => $this->model_cupom->getCategoria(),
                'setor' => $this->model_cupom->getSetor(),
                'data_inicio' => $this->model_cupom->getDataInicio(),
                'data_termino' => $this->model_cupom->getDataTermino()
            ];
            $this->form_cupom->populate($formata_dados);
        }

        if( $this->getRequest()->isPost() ) {
            if( $this->form_cupom->isValid($this->getRequest()->getPost()) ) {
                //verifica se foi realizado o upload da imagem
                if( $nome_imagem = $this->model_util->uploadImagem('images/produtos/') ) {

                    $dados = $this->form_cupom->getValues();
                    //seta os dados do formulario para o model
                    $this->model_cupom->setId($dados['id']);
                    $this->model_cupom->setIdParceiro($dados['idParceiro']);
                    $this->model_cupom->setBreveDescricao($dados['breve_descricao']);
                    $this->model_cupom->setDetalhes($dados['detalhes']);
                    $this->model_cupom->setDe($dados['de']);
                    $this->model_cupom->setPor($dados['por']);
                    $this->model_cupom->setLocais($dados['locais']);
                    $this->model_cupom->setQuantidade($dados['quantidade']);
                    $this->model_cupom->setCategoria($dados['categoria']);
                    $this->model_cupom->setImagem($nome_imagem);
                    $this->model_cupom->setSetor($dados['setor']);
                    $this->model_cupom->setDataInicio($dados['data_inicio']);
                    $this->model_cupom->setDataTermino($dados['data_termino']);

                    //atualiza as informacoes do cupom
                    if( $this->model_cupom->atualizarDadosCupom() ) {
                        $mensagem = $this->mensagem(
                                'Cupom editado com sucesso!', 'sucesso'
                            );
                    } else {
                        $mensagem = $this->mensagem(
                                'Não foi possível editar o cupom. Tente novamente mais tarde!', 'erro'
                            );
                    }
                } else {
                     $mensagem = $this->mensagem(
                                'Não foi possível cadastrar a imagem do cupom. Tente novamente mais tarde!', 'erro'
                            );
                }
                //direciona para index e assina a mensagem de retorno
                $this->forward('index', 'cupom', 'admin', $mensagem);
            }
        }

        $this->view->assign('form', $this->form_cupom);
    }

    public function excluirAction()
    {
    	if( $id = $this->getRequest()->getParam('id') ) {
            if( $this->model_cupom->excluirCupom( $id ) ) {
                $mensagem = $this->mensagem('Cupom excluído com sucesso!', 'sucesso');
            } else {
                $mensagem = $this->mensagem('Não foi possível excluir o cupom. Tente novamente mais tarde!', 'erro');
            }
            $this->forward('index', 'cupom', 'admin', $mensagem);
        }
    }

    public function mensagem( $mensagem, $status = null ) 
    {
        switch ($status) {
            case 'sucesso':
                $retorno = [
                    'status_alerta' => 'alert-success',
                    'mensagem' => $mensagem
                ];
                break;
            case 'erro':
                $retorno = [
                    'status_alerta' => 'alert-error',
                    'mensagem' => $mensagem
                ];
                break;
            default:
                $retorno = [
                    'status_alerta' => 'alert-info',
                    'mensagem' => $mensagem
                ];
                break;
        }
        return $retorno;
    }

}
<?php

class Main_Model_Util
{

	private $rand;

	/**
	* Método construtor para instânciar a conexão com banco de dados
	* @name _construct()
	* @return void
	*/
	public function __construct() {
		$this->db = Zend_Registry::get('db');
	}

	/**
	* Gera numero randomico para registrar ao gerar o cupom do participante
	*
	* @name rand()
	* @return array
	*/
	public function rand()
	{
		$return = false;
		while ( $return == false ) {

			$this->rand = rand(20, 100000);

			$sql = $this->db->select()
					    ->from('cupons_participantes')
					    ->where('rand = ?', $this->rand);
		
			$result = $this->db->fetchAll($sql);

			if( !$result ) {
				$return = true;
				return $this->rand;
			}
		}

	}

}
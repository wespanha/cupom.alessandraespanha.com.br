<?php

class Main_Model_Cupom extends Main_Model_Util
{

	private $rand;

	/**
	* Método construtor para instânciar a conexão com banco de dados
	* @name _construct()
	* @return void
	*/
	public function __construct() {
		$this->db = Zend_Registry::get('db');
	}

	/**
	* Busca as categorias e quantidade de cupons para cada
	*
	* @name buscarCategorias()
	* @param $slug [nome amigavel da categoria a ser pesquisada] (Opcional)
	* @return array [$result]
	*/
	public function buscarCategorias( $slug = null )
	{
		$sql = $this->db->select()
						->from('categorias')
						->order('slug');

		if( !is_null($slug) ) {
			$sql->where('slug = ?', $slug);
			return $this->db->fetchRow($sql);
		}

		$result = $this->db->fetchAll($sql);
		foreach ($result as $key => $value) {
			$cont = $this->contadorCupomPorCategoria($value['slug']);
			$result[$key]['contador'] = ($cont['quantidade'] == 0 || $cont['quantidade'] == 1) ? $cont['quantidade'].' cupom' : $cont['quantidade'].' cupons';
		}

		return $result;
	}

	/**
	* Busca todos os cupons dentro do intervalo de datas e categorias
	*
	* @name buscarTodosCuponsDentroDaDataECategoria()
	* @param $categoria [nome amigavel da categoria a ser filtrada] (Obrigatório)
	* @param $idParticipante [excluir da listagem os cupons ja resgatados pelo usuario] (Opcional)
	* @return array [$result]
	*/
	public function buscarTodosCuponsDentroDaDataECategoria( $categoria, $idParticipante = null )
	{
		$data = Date('Y-m-d');
		$sql = $this->db->select()
					    ->from('cupons')
					    ->join('parceiros', 'parceiros.id = cupons.idParceiro', array('nome', 'logo'))
					    ->where('categoria = ?', $categoria)
					    ->where('data_inicio <= ?', $data)
					    ->where('data_termino >= ?', $data)
					    ->where('setor = "pegolevo"')
					    ->order(array('parceiros.nome', 'cupons.id DESC'));

		//exclui dos cupons disponiveis os que ja foram gerados pelo participante
		if( !is_null( $idParticipante ) ) {

			$sql1 = $this->db->select()
						    ->from('cupons_participantes', array('idCupom'))
						    ->where('idParticipante = ?', $idParticipante);
			$result = $this->db->fetchAll($sql1);

			//se existe algum cupom resgatado pelo participante
			if( $result ) {
				$arr = array();
				foreach ($result as $key => $value) {
					$arr[] = $value['idCupom'];
				}
				//exclui da listagem os cupons ja resgatados pelo participante
				$sql->where('cupons.id NOT IN (?)', $arr);
			}

		}
		
		$result = $this->db->fetchAll($sql);

		if( $result ) {
			foreach ($result as $key => $value) {
				$result[$key]['disponibilidade'] = $this->verificaQuantidadeDeCupomDisponivel( $value['id'] );
			}
			return $result;
		}

		return false;
	}

	/**
	* Busca todos os cupons dentro do intervalo de datas e categorias
	*
	* @name buscarCupomOuCupomGerado()
	* @param $idCupom [id do cupom caso seja filtrado] (Opcional)
	* @param $rand [codigo do cupom gerado por um usuario caso seja filtrado] (Opcional)
	* @return array [$result]
	*/
	public function buscarCupomOuCupomGerado( $idCupom = null, $rand = null )
	{
		//buscar um cupom
		if( !is_null($idCupom) ) {

			$sql = $this->db->select()
					    ->from('cupons')
					    ->join('parceiros', 'parceiros.id = cupons.idParceiro', array('nome', 'logo'))
					    ->where('cupons.id = ?', $idCupom);
		//buscar um cupom gerado
		} elseif ( !is_null($rand) ) {

			$sql = $this->db->select()
						->from('cupons_participantes', array('*','DATEDIFF(NOW(), cupons_participantes.data) AS dias_corridos', 'data AS data_cadastro'))
						->join('cupons', 'cupons.id = cupons_participantes.idCupom')
						->join('participantes', 'participantes.id = cupons_participantes.idParticipante')
						->join('parceiros', 'parceiros.id = cupons.idParceiro', 
								array('nome AS nomeParceiro', 'logo'))
						->where('cupons_participantes.rand = ?', $rand);
						
		}

		$result = $this->db->fetchRow($sql);

		if( $result ) {
			return $result;
		}

		return false;
	}

	/**
	* Busca todos os cupons de um participante desejado
	*
	* @name buscarTodosCuponsDeUmParticipante()
	* @param $idParticipante [id do participante] (Obrigatório)
	* @return array [$result]
	*/
	public function buscarTodosCuponsDeUmParticipante( $idParticipante )
	{
		$sql = $this->db->select()
					    ->from('cupons_participantes')
					    ->join('cupons', 'cupons.id = cupons_participantes.idCupom', array('detalhes','imagem','breve_descricao','de','por','setor'))
					    ->join('parceiros', 'parceiros.id = cupons.idParceiro', array('nome', 'logo'))
					    ->where('idParticipante = ?', $idParticipante)
					    ->order('cupons_participantes.id DESC');
		
		$result = $this->db->fetchAll($sql);

		if( $result ) {
			return $result;
		}

		return false;
	}

	/**
	* Verifica a relação entre cupom e participante
	*
	* @name buscaRelacaoEntreCupomEParticipante()
	* @param $idParticipante [id do participante] (Obrigatório)
	* @param $idCupom [id do cupom] (Obrigatório)
	* @return bool
	*/
	public function buscaRelacaoEntreCupomEParticipante( $idParticipante, $idCupom )
	{
		$sql = $this->db->select()
					    ->from('cupons_participantes')
					    ->where('idParticipante = ?', $idParticipante)
					    ->where('idCupom = ?', $idCupom);
		
		$result = $this->db->fetchRow($sql);

		if( $result ) {
			return $result['rand'];
		}

		return false;
	}

	/**
	* Gera o cupom relacionando com o participante (usuário)
	*
	* @name gerarCupom()
	* @param array $dados [dados do participante] (Obrigatório)
	* @param $idCupom [id do cupom] (Obrigatório)
	* @return string [retorna o valor randomico do cupom do participante]
	*/
	public function gerarCupom( $dados, $idCupom )
	{
		//gera o numero randomico do cupom
		$this->rand = $this->rand();

		if( $this->rand() && !$this->buscaRelacaoEntreCupomEParticipante($dados->getId(), $idCupom) ) {
		
			$data = array(
				'idParticipante' => $dados->getId(),
				'idCupom' => $idCupom,
				'rand' => $this->rand,
				'data' => Date('Y-m-d')
			);

			if( $this->db->insert('cupons_participantes', $data) )
				return $this->rand;
		}

		return false;
		
	}

	/**
	* Verifica se o cupom está no prazo de validade e se foi validado
	*
	* @name validarCupom()
	* @param $rand [numero randomico do cupom gerado pelo participante] (Obrigatório)
	* @return bool
	*/
	public function validarCupom( $rand )
	{	
		//busca o cupom do participante e verifica se esta dentro da data
		$sql = $this->db->select()
					    ->from('cupons_participantes')
					    ->where('validado = "n"')
					    ->where('rand = ?', $rand)
					    ->where('DATEDIFF(NOW(), data) <= 3');
		$result = $this->db->fetchRow($sql);

		if( $result ) {
			//verifica se o cupom ja foi validado e se existe quantidade de cupom disponivel
			if( $result && $this->verificaQuantidadeDeCupomDisponivel($result['idCupom'], true) ) {
				$dados['data_validado'] = Date('Y-m-d');
				$dados['validado'] = 's';

				$where = $this->db->quoteInto('rand = ?', $rand);

				if( $this->db->update('cupons_participantes', $dados, $where) )
					return true;
			}
		}
	
		return false;
	}

	/**
	* Verifica a quantidade de cupons por categoria
	*
	* @name contadorCupomPorCategoria()
	* @param $categoria [nome amigavel da categoria a ser filtrada] (Obrigatório)
	* @return array
	*/
	public function contadorCupomPorCategoria( $categoria )
	{
		$data = Date('Y-m-d');
		$sql = $this->db->select()
						->from('cupons', array('COUNT(*) AS quantidade'))
						->where('data_inicio <= ?', $data)
					    ->where('data_termino >= ?', $data)
						->where('cupons.categoria = ?', $categoria)
						->where('cupons.setor = "pegolevo"');

		$result = $this->db->fetchRow($sql);

		return $result;		
	}

	/**
	* Verifica a quantidade de cupons de um participante, cupons não validados e validados
	*
	* @name contadorCupomParticipante()
	* @param $idParticipante [id do participante a ser filtrado] (Obrigatório)
	* @param $validado [se esta buscando cupons validados ou nao] (Opcional)
	* @return array
	*/
	public function contadorCupomParticipante( $idParticipante, $validado = null )
	{
		$sql = $this->db->select()
					    ->from('cupons_participantes')
					    ->where('idParticipante = ?', $idParticipante);

		if( !is_null($validado) ) {
			$sql->where('validado = "s"');
		}

		$result = $this->db->fetchAll($sql);
		return count($result);
	}

	/**
	* Verifica a disponibilidade de cupons a serem resgatados, levando em consideracao os que foram gerados
	*
	* @name verificaQuantidadeDeCupomDisponivel()
	* @param $idCupom [id cupom a ser verificado] (Obrigatório)
	* @param $atendente [verifica se esta sendo verificado por um atendente] (Opcional)
	* @return bool
	*/
	public function verificaQuantidadeDeCupomDisponivel( $idCupom, $atendente = false )
	{
		$cupom = $this->buscarCupomOuCupomGerado( $idCupom );

		$sql = $this->db->select()
					    ->from('cupons_participantes')
					    ->join('cupons', 'cupons_participantes.idCupom = cupons.id')
					    ->where('cupons.setor = "pegolevo"')
					    ->where('idCupom = ?', $idCupom);

		//caso o atendente esteja verificando deve retornar se o cupom esta vencido
		if( !$atendente ) {
			$sql->where('DATEDIFF(NOW(), data) <= 3 OR validado = "s"');
		//se o participante estiver consultando, verifia somente se foi validado
		} else {
			$sql->where('validado = "s"');
		}

		$participantes_cupons = $this->db->fetchAll($sql);
		
		if(count($participantes_cupons) < $cupom['quantidade']) {
			return true;
		}
		return false;
	}

}
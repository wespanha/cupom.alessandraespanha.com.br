<?php

class Main_Model_Desapega extends Main_Model_Util
{

	private $rand;

	/**
	* Método construtor para instânciar a conexão com banco de dados
	* @name _construct()
	* @return void
	*/
	public function __construct() {
		$this->db = Zend_Registry::get('db');
	}

	public function buscarTodosCuponsDentroDaData( $idParticipante = null )
	{
		$date = Date('Y-m-d');
		$sql = $this->db->select()
					    ->from('cupons')
					    ->join('parceiros', 'parceiros.id = cupons.idParceiro', array('nome', 'logo'))
					    ->where('data_inicio <= ?', $date)
					    ->where('data_termino >= ?', $date)
					    ->where('setor = "desapega"')
					    ->order('id DESC');

		//exclui dos cupons disponiveis os que ja foram gerados pelo participante
		if( !is_null( $idParticipante ) ) {

			$sql1 = $this->db->select()
						    ->from('cupons_participantes', array('idCupom'))
						    ->where('idParticipante = ?', $idParticipante);
			$result = $this->db->fetchAll($sql1);

			//se existe algum cupom resgatado pelo participante
			if( $result ) {
				$arr = array();
				foreach ($result as $key => $value) {
					$arr[] = $value['idCupom'];
				}
				//exclui da listagem os cupons ja resgatados pelo participante
				$sql->where('cupons.id NOT IN (?)', $arr);
			}

		}
		
		$result = $this->db->fetchAll($sql);

		if( $result ) {
			return $result;
		}

		return false;
	}

	public function buscarCupomOuCupomGerado( $idCupom = null, $rand = null )
	{
		//buscar um cupom
		if( !is_null($idCupom) ) {

			$sql = $this->db->select()
					    ->from('cupons')
					    ->join('parceiros', 'parceiros.id = cupons.idParceiro', array('nome', 'logo'))
					    ->where('cupons.id = ?', $idCupom);
		//buscar um cupom gerado
		} elseif ( !is_null($rand) ) {

			$sql = $this->db->select()
						->from('cupons_participantes', array('*','DATEDIFF(NOW(), cupons_participantes.data) AS dias_corridos', 'data AS data_cadastro'))
						->join('cupons', 'cupons.id = cupons_participantes.idCupom')
						->join('participantes', 'participantes.id = cupons_participantes.idParticipante')
						->join('parceiros', 'parceiros.id = cupons.idParceiro', 
								array('nome AS nomeParceiro', 'logo'))
						->where('cupons_participantes.rand = ?', $rand);
						
		}

		$result = $this->db->fetchRow($sql);

		if( $result ) {
			return $result;
		}

		return false;
	}

	public function buscarTodosCuponsDeUmParticipante( $idParticipante )
	{
		$sql = $this->db->select()
					    ->from('cupons_participantes')
					    ->join('cupons', 'cupons.id = cupons_participantes.idCupom', array('detalhes','imagem','breve_descricao','de','por'))
					    ->join('parceiros', 'parceiros.id = cupons.idParceiro', array('nome', 'logo'))
					    ->where('idParticipante = ?', $idParticipante)
					    ->where('cupons.setor = "desapega"');
		
		$result = $this->db->fetchAll($sql);

		if( $result ) {
			return $result;
		}

		return false;
	}

	public function buscaRelacaoEntreCupomEParticipante( $idParticipante, $idCupom )
	{
		$sql = $this->db->select()
					    ->from('cupons_participantes')
					    ->where('idParticipante = ?', $idParticipante)
					    ->where('idCupom = ?', $idCupom);
		
		$result = $this->db->fetchRow($sql);

		if( $result ) {
			return true;
		}

		return false;
	}

	public function gerarCupom( $dados, $idCupom )
	{
		//gera o numero randomico do cupom
		$this->rand = $this->rand();

		if( $this->rand() && !$this->buscaRelacaoEntreCupomEParticipante($dados->getId(), $idCupom) ) {
		
			$data = array(
				'idParticipante' => $dados->getId(),
				'idCupom' => $idCupom,
				'rand' => $this->rand,
				'data' => Date('Y-m-d')
			);

			if( $this->db->insert('cupons_participantes', $data) )
				return $this->rand;
		}

		return false;
		
	}

	public function validarCupom( $rand )
	{	
		//busca o cupom do participante e verifica se esta dentro da data
		$sql = $this->db->select()
					    ->from('cupons_participantes')
					    ->where('validado = "n"')
					    ->where('rand = ?', $rand)
					    ->where('DATEDIFF(NOW(), data) <= 3');
		$result = $this->db->fetchRow($sql);

		if( $result ) {
			//verifica se o cupom ja foi validado e se existe quantidade de cupom disponivel
			if( $result && $this->verificaQuantidadeDeCupomDisponivel($result['idCupom'], true) ) {
				$dados['data_validado'] = Date('Y-m-d');
				$dados['validado'] = 's';

				$where = $this->db->quoteInto('rand = ?', $rand);

				if( $this->db->update('cupons_participantes', $dados, $where) )
					return true;
			}
		}
	
		return false;
	}

	public function contadorCupomParticipante( $idParticipante, $validado = null )
	{
		$sql = $this->db->select()
					    ->from('cupons_participantes')
					    ->where('idParticipante = ?', $idParticipante);

		if( !is_null($validado) ) {
			$sql->where('validado = "s"');
		}

		$result = $this->db->fetchAll($sql);
		return count($result);
	}

	public function verificaQuantidadeDeCupomDisponivel( $idCupom, $atendente = false )
	{
		$cupom = $this->buscarCupomOuCupomGerado( $idCupom );

		$sql = $this->db->select()
					    ->from('cupons_participantes')
					    ->join('cupons', 'cupons_participantes.idCupom = cupons.id')
					    ->where('cupons.setor = "desapega"')
					    ->where('idCupom = ?', $idCupom);

		if( !$atendente ) {
			$sql->where('DATEDIFF(NOW(), data) <= 3 OR validado = "s"');
		} else {
			$sql->where('validado = "s"');
		}

		$participantes_cupons = $this->db->fetchAll($sql);
		
		if(count($participantes_cupons) < $cupom['quantidade']) {
			return true;
		}
		return false;
	}

}
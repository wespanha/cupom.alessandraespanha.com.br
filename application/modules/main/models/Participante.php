<?php

class Main_Model_Participante extends Main_Model_Util
{

	private $rand;

	/**
	* Método construtor para instânciar a conexão com banco de dados
	* @name _construct()
	* @return void
	*/
	public function __construct() {
		$this->db = Zend_Registry::get('db');
	}

	/**
	* Cria usuario para o participante
	*
	* @name criarParticipante()
	* @param $dados [dados retornados do formulario de cadastro] (Obrigatório)
	* @return bool
	*/
	public function criarParticipante( $dados )
	{	
		$dados['data'] = Date('Y-m-d');

		$sql = $this->db->select()
					    ->from('participantes')
					    ->where('email = ?', $dados['email'])
					    ->orWhere('cpf = ?', $dados['cpf']);
		
		$result = $this->db->fetchRow($sql);
		
		if( !$result ) {
			unset($dados['idCupom']);
			unset($dados['setor']);
			unset($dados['confirma_termo']);
			if( $this->db->insert('participantes', $dados) )
				return true;
		}
		
		return false;
	}

	/**
	* Retorna os dados do usuario cadastrado na plataforma
	*
	* @name buscarDadosParticipante()
	* @param $id [id do usuario a ser filtrado] (Obrigatório)
	* @return array
	*/
	public function buscarDadosParticipante( $id )
	{
		$sql = $this->db->select()
					    ->from('participantes')
					    ->where('id = ?', $id);
		
		$result = $this->db->fetchRow($sql);
		
		if( $result ) {
			return $result;
		}
		
		return false;
	}

}
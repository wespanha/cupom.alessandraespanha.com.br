<?php

class Main_Model_Atendente
{

	/**
	* Método construtor para instânciar a conexão com banco de dados
	* @name _construct()
	* @return void
	*/
	public function __construct() {
		$this->db = Zend_Registry::get('db');
	}

	/**
	* Busca todos os dados do cupom e verifica relacao do atendente e da loja
	*
	* @name loginAtendente()
	* @param $dados [dados retornados do formulario de login] (Obrigatório)
	* @return array
	*/
	public function loginAtendente( $dados )
	{
		$arr = array();
		$sql = $this->db->select()
						->from('cupons_participantes', array('*', 'data AS data_cadastro'))
						->join('cupons', 'cupons.id = cupons_participantes.idCupom')
						->join('participantes', 'participantes.id = cupons_participantes.idParticipante')
						->join('parceiros', 'parceiros.id = cupons.idParceiro', 
								array('nome AS nomeParceiro', 'logo'))
						->where('cupons_participantes.rand = ?', $dados['id']);
		$result = $this->db->fetchRow($sql);

		//se existe dados para o cupom informado verifica o login do atendente
		if( $result ) {
			$arr = $result;
			$sql = $this->db->select()
						    ->from('atendentes')
						    ->where('nomeDeUsuario = ?', $dados['nomedeusuario'])
						    ->where('senha = ?', sha1($dados['senha']))
						    ->where('idParceiro = ?', $result['idParceiro']);
			$result = $this->db->fetchRow($sql);

			if( $result ) {
				$arr['atendente'] = $result;
				return $arr;
			}
		}

		return false;
		
	}

}
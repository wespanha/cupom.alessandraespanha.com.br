<?php

class Main_Form_Divulgue extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$nome = new Zend_Form_Element_Text('nome');
		$nome->removeDecorator('Label')
	   		 ->removeDecorator('HtmlTag')
	   		 ->setRequired()
			 ->setErrorMessages(array('Campo obrigatório'))
			 ->setAttribs(array('placeholder' => 'Nome Completo',
			 					'required' => ''));
		$this->addElement($nome);

		$email = new Zend_Form_Element_Text('email');
		$email->removeDecorator('Label')
	   		  ->removeDecorator('HtmlTag')
	   		  ->setRequired()
			  ->setErrorMessages(array('Campo obrigatório'))
			  ->setAttribs(array('placeholder' => 'E-mail',
			 					'required' => ''))
			  ->addValidator('EmailAddress', TRUE);
		$this->addElement($email);

		$produto = new Zend_Form_Element_Text('produto');
		$produto->removeDecorator('Label')
	   		    ->removeDecorator('HtmlTag')
	   		    ->setRequired()
			    ->setErrorMessages(array('Campo obrigatório'))
			    ->setAttribs(array('placeholder' => 'Produto',
			 					'required' => ''));
		$this->addElement($produto);

		$descricao = new Zend_Form_Element_Textarea('descricao');
		$descricao->removeDecorator('Label')
		   		  ->removeDecorator('HtmlTag')
		   		  ->setRequired()
				  ->setErrorMessages(array('Campo obrigatório'))
				  ->setAttribs(array('placeholder' => 'Descrição do produto',
				 					'required' => '',
				 					'rows' => '1'));
		$this->addElement($descricao);

		$endereco = new Zend_Form_Element_Text('endereco');
		$endereco->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('placeholder' => 'Endereço',
				 					'required' => ''));
		$this->addElement($endereco);

		$cidade = new Zend_Form_Element_Text('cidade');
		$cidade->removeDecorator('Label')
		   	   ->removeDecorator('HtmlTag')
		   	   ->setRequired()
			   ->setErrorMessages(array('Campo obrigatório'))
			   ->setAttribs(array('placeholder' => 'Cidade',
				 					'required' => ''));
		$this->addElement($cidade);

		$valor = new Zend_Form_Element_Text('valor');
		$valor->removeDecorator('Label')
	   		  ->removeDecorator('HtmlTag')
	   		  ->setRequired()
			  ->setErrorMessages(array('Campo obrigatório'))
			  ->setAttribs(array('placeholder' => 'Valor do produto (R$)',
			 					 'required' => ''));
		$this->addElement($valor);

		$imagem = new Zend_Form_Element_File('imagem');
		$imagem->removeDecorator('Label')
			   ->removeDecorator('HtmlTag')
			   ->setErrorMessages(array('Campo obrigatório'))
			   ->setDestination('upload/');
		$this->addElement($imagem);

	}

}
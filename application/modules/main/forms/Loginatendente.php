<?php

class Main_Form_Loginatendente extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$nomedeusuario = new Zend_Form_Element_Text('nomedeusuario');
		$nomedeusuario->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('placeholder' => 'Nome de usuário',
				 					'required' => ''));
		$this->addElement($nomedeusuario);

		$senha = new Zend_Form_Element_Password('senha');
		$senha->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('placeholder' => 'Senha',
				 					'required' => ''));
		$this->addElement($senha);

	}

}
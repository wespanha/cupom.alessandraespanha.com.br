<?php

class Main_Form_Login extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$idCupom = new Zend_Form_Element_Hidden('idCupom');
		$idCupom->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($idCupom);

		$setor = new Zend_Form_Element_Hidden('setor');
		$setor->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($setor);

		$email = new Zend_Form_Element_Text('email');
		$email->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('placeholder' => 'E-mail',
				 					'required' => ''))
				 ->addValidator('EmailAddress', true);
		$this->addElement($email);

		$cpf = new Zend_Form_Element_Text('cpf');
		$cpf->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('placeholder' => 'CPF',
				 					'class' => 'cpf',
				 					'required' => ''));
		$this->addElement($cpf);

	}

}
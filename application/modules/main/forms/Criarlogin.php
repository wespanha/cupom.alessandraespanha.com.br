<?php

class Main_Form_Criarlogin extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$idCupom = new Zend_Form_Element_Hidden('idCupom');
		$idCupom->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($idCupom);

		$setor = new Zend_Form_Element_Hidden('setor');
		$setor->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($setor);

		$nome = new Zend_Form_Element_Text('nome');
		$nome->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('placeholder' => 'Nome Completo',
				 					'required' => ''));
		$this->addElement($nome);

		$email = new Zend_Form_Element_Text('email');
		$email->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('placeholder' => 'E-mail',
				 					'required' => ''))
				 ->addValidator('EmailAddress', true);
		$this->addElement($email);

		$celular = new Zend_Form_Element_Text('celular');
		$celular->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('placeholder' => 'Celular',
				 					'class' => 'telefone',
				 					'required' => ''));
		$this->addElement($celular);

		$cpf = new Zend_Form_Element_Text('cpf');
		$cpf->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('placeholder' => 'CPF',
				 					'class' => 'cpf',
				 					'required' => ''));
		$this->addElement($cpf);

		$confirma_termo = new Zend_Form_Element_Checkbox('confirma_termo');
		$confirma_termo->removeDecorator('HtmlTag')
					   ->removeDecorator('Label')
					   ->setAttribs(array('id' => 'confirma_termo',
										  'required' => ''))
					   ->setCheckedValue();
		$this->addElement($confirma_termo);

	}

}
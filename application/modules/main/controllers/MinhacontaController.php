<?php

class Main_MinhacontaController extends Zend_Controller_Action
{

    /**
    * armazena o formulario de login
    */
	private $form_login;

    /**
    * armazena o formulario para criar login
    */
    private $form_criarlogin;

    /**
    * armazena os dados do usuario logado no sistema
    */
    private $dados_usuario;

    /**
    * armazena o model com os processos do cupom
    */
    private $model_cupom;

    /**
    * armazena o model com os processos do participante
    */
    private $model_participante;


    public function init()
    {
        //busca dados registrados do usuario logado no sistema
        $this->dados_usuario = Zend_Auth::getInstance()->getStorage()->read();
        //assina pra view os dados de usuario
        $this->view->assign('dados_usuario', $this->dados_usuario);
        //model de cupons
        $this->model_cupom = new Main_Model_Cupom();
        //model participantes
        $this->model_participante = new Main_Model_Participante();
        //formaulario para login e criacao de usuario
    	$this->form_login = new Main_Form_Login();
        //formulario para criar login
        $this->form_criarlogin = new Main_Form_Criarlogin();
    }

    public function indexAction()
    {    
    	//exibe mensagem de erro quando ocorrer
        if( $this->getRequest()->getParam('mensagem') ) {
            $valor = $this->getRequest()->getParam('mensagem');
            //mensagem de erro no momento do login
            if( $valor == 'erro_login' ) {
                $this->view->assign('mensagem', 'E-mail e CPF não correspondem. Tente novamente!');
            }
        }

        if( $this->getRequest()->getParam('idCupom') ) {
            $this->form_login->idCupom->setValue( $this->getRequest()->getParam('idCupom') );
            $this->view->assign('idCupom', $this->getRequest()->getParam('idCupom'));
        }

        if( $this->getRequest()->getParam('setor') ) {
            $this->form_login->setor->setValue( $this->getRequest()->getParam('setor') );
            $this->view->assign('setor', $this->getRequest()->getParam('setor'));
        }

    	//verifica se foi submetido o formulario
		if( $this->getRequest()->isPost() ) {
            //seta os dados do formulario
            $form_data = $this->getRequest()->getPost();
            
            if( $this->form_login->isValid($form_data) ){
                //valida o campo cpf
                if( strlen($form_data['cpf']) != 14 ) {
                    $this->redirect('/minhaconta/index/mensagem/erro_login/');
                    return false;
                }
    			//verifica se usuário e senha estão corretos
    			$auth = Plugins_Auth_AuthSetup::login($form_data['email'], $form_data['cpf']);
    			if( $auth ) {
                    //se tem idCupom direciona para os detalhes do cupom
                    if( $form_data['idCupom'] != '' ) {
                        $this->redirect('/'.$form_data['setor'].'/detalhe/id/'.$form_data['idCupom']);
                    }
                    //direciona para a tela inicial da conta
    				$this->redirect('/minhaconta/detalhe/');
    			} else {
                    $this->redirect('/minhaconta/index/mensagem/erro_login/');
    			}
            } else {
                $this->redirect('/minhaconta/index/mensagem/erro_login/');
            }
			
		}

   		$this->view->assign('form', $this->form_login);
    }

    public function criarcontaAction()
    {
        //exibe mensagem de erro quando ocorrer
        if( $this->getRequest()->getParam('mensagem') ) {
            $valor = $this->getRequest()->getParam('mensagem');
            //mensagem de erro no momento do cadastro
            if( $valor == 'erro_cadastro' ) {
                $this->view->assign('mensagem', 'Não foi possível criar usuário. Verifique os dados preenchidos e tente novamente!');
            }
        }

        if( $this->getRequest()->getParam('idCupom') ) {
            $this->form_criarlogin->idCupom->setValue( $this->getRequest()->getParam('idCupom') );
        }

        if( $this->getRequest()->getParam('setor') ) {
            $this->form_criarlogin->setor->setValue( $this->getRequest()->getParam('setor') );
        }

        //verifica se foi submetido o formulario
        if( $this->getRequest()->isPost() ) {
            //seta os dados do formulario
            $form_data = $this->getRequest()->getPost();
            
            if( $this->form_criarlogin->isValid($form_data) ) {

                if( !strstr($form_data['email'], "@") ) {
                    $this->redirect('/minhaconta/criarconta/mensagem/erro_cadastro/');
                    return false;
                }

                if( strlen($form_data['cpf']) != 14 ) {
                    $this->redirect('/minhaconta/criarconta/mensagem/erro_cadastro/');
                    return false;
                }

                if( strlen($form_data['celular']) != 15 ) {
                    $this->redirect('/minhaconta/criarconta/mensagem/erro_cadastro/');
                    return false;
                }

                //verifica se foi possivel criar o usuario
                if( $this->model_participante->criarParticipante($form_data) ) {
                    $auth = Plugins_Auth_AuthSetup::login($form_data['email'], $form_data['cpf']);
                    if( $auth ) {
                        //se tem idCupom direciona para os detalhes do cupom
                        if( $form_data['idCupom'] != '' ) {
                            $this->redirect('/'.$form_data['setor'].'/detalhe/id/'.$form_data['idCupom']);
                        }
                        //direciona para a tela inicial da conta
                        $this->redirect('/minhaconta/detalhe/');
                    } else {
                        $this->redirect('/minhaconta/index/mensagem/erro_login/');
                    }    
                } else {
                    $this->redirect('/minhaconta/criarconta/mensagem/erro_cadastro/');
                }
            } else {
                $this->redirect('/minhaconta/criarconta/mensagem/erro_cadastro/');
            }
            
        }

        $this->view->assign('form', $this->form_criarlogin);
    }

    public function editarcontaAction()
    {
        if( $this->getRequest()->getParam('id') ) {
            $id = $this->getRequest()->getParam('id');
            $dados = $this->model_participante->buscarDadosParticipante( $id );

            $this->form_criarlogin->populate($dados);
        }

        //verifica se foi submetido o formulario
        if( $this->getRequest()->isPost() ) {
            //seta os dados do formulario
            $form_data = $this->getRequest()->getPost();
            
            if( $this->form_criarlogin->isValid($form_data) ){
                
                $config = array('ssl' => 'ssl',
                                'auth' => 'login',
                                'username' => 'desenvolvimento@wespanha.com.br',
                                'port' => '465',
                                'password' => 'devwespanha');

                $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);

                $html = "<p>Nome: ".$form_data['nome']."</p>";
                $html .= "<p>Email: ".$form_data['email']."</p>";
                $html .= "<p>Celular: ".$form_data['celular']."</p>";
                $html .= "<p>CPF: ".$form_data['cpf']."</p>";

                $mail = new Zend_Mail('utf8');
                $mail->setBodyHtml($html);
                $mail->setFrom('desenvolvimento@wespanha.com.br', 'Contato Site');
                $mail->addTo('fernando.moscardini@wespanha.com.br');
                $mail->setSubject('Solicitação de alteração do perfil');

                if( $mail->send($transport) ) {
                    $this->redirect('/minhaconta/detalhe/mensagem/perfil_alteracao/');
                }
            }
        }
        $this->view->assign('form', $this->form_criarlogin);
    }

    public function detalheAction()
    {    
        //exibe mensagem de erro quando ocorrer
        if( $this->getRequest()->getParam('mensagem') ) {
            $valor = $this->getRequest()->getParam('mensagem');
            //mensagem de sucesso no envio da alteracao do perfil
            if( $valor == 'perfil_alteracao' ) {
                $this->view->assign('mensagem', 'Sua solicitação de alteração do perfil foi enviada.');
            }
        }

        //lista todos os cupons resgatados por um participante
        $cupons_participantes = $this->model_cupom->buscarTodosCuponsDeUmParticipante($this->dados_usuario->getId());
        $this->view->assign('cupons_participantes', $cupons_participantes);
        //total de cupons resgatados
        $contadorCupomParticipante = $this->model_cupom->contadorCupomParticipante($this->dados_usuario->getId());
        $this->view->assign('contadorCupons', $contadorCupomParticipante);
        //total de cupons resgatados e validados pelo atendente
        $contadorCupomParticipanteValidado = $this->model_cupom->contadorCupomParticipante($this->dados_usuario->getId(),true);
        $this->view->assign('contadorCuponsValidados', $contadorCupomParticipanteValidado);
    }

    public function meuscuponsAction()
    {
        //lista todos os cupons resgatados por um participante
        $cupons_participantes = $this->model_cupom->buscarTodosCuponsDeUmParticipante($this->dados_usuario->getId());
        $this->view->assign('cupons_participantes', $cupons_participantes);
    }

    public function meucupomAction() 
    {
        //assina para view a mensagem de cupom gerado
        if( $this->getRequest()->getParam('mensagem') ) {
            $this->view->assign('mensagem', 'Parabéns! Você gerou o cupom.');
        }
        
        //busca os dados do cupom para assinar na view
        if( $this->getRequest()->getParam('rand') ) {
            $this->view->assign('dados', $this->model_cupom->buscarCupomOuCupomGerado( null, $this->getRequest()->getParam('rand') ));
        } else {
            $this->redirect('/minhaconta');
        }
    }

    /**
    * Tela para formatacao da pagina do cupom a ser impresso
    */
    public function printcupomAction()
    {
        //tela para impressão do código
        $this->_helper->layout->disableLayout();

        if( $this->getRequest()->getParam('rand') ) {
            $this->view->assign('dados', $this->model_cupom->buscarCupomOuCupomGerado( null, $this->getRequest()->getParam('rand') ));
        } else {
            $this->redirect('/minhaconta');
        }
    }

    public function logoutAction() 
    {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();

        $this->redirect('/');
    }

}


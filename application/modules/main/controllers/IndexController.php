<?php

class Main_IndexController extends Zend_Controller_Action
{

    /**
    * armazena os dados do usuario logado no sistema
    */
    private $dados_usuario;

    private $model_cupom;

    private $model_desapega;

    public function init()
    {
        //busca dados registrados do usuario logado no sistema
        $this->dados_usuario = Zend_Auth::getInstance()->getStorage()->read();
        //model dos cupons
        $this->model_cupom = new Main_Model_Cupom();
        //model desapega
        $this->model_desapega = new Main_Model_Desapega();
    }

    public function indexAction()
    {    
        $categorias = $this->model_cupom->buscarCategorias();
        $this->view->assign('categorias', $categorias);

        $desapega = $this->model_desapega->buscarTodosCuponsDentroDaData( $this->dados_usuario->getId() );
        $this->view->assign('desapega', $desapega);
    }

}


<?php

class Main_DesapegaController extends Zend_Controller_Action
{
    /**
    * armazena os dados do usuario logado no sistema
    */
    private $dados_usuario;

    /**
    * armazena o model com os processos do setor desapega
    */
    private $model_desapega;

    /**
    * armazena a instacia do layout
    */
    private $layout;

    /**
    * armazena o formulario de login do atendente no sistema para validacao
    */
    private $form_loginatendente;

    public function init()
    {
        //busca dados registrados do usuario logado no sistema
        $this->dados_usuario = Zend_Auth::getInstance()->getStorage()->read();
        //model de cupons
        $this->model_desapega = new Main_Model_Desapega();
        //formulario para login do atendente
        $this->form_loginatendente = new Main_Form_Loginatendente();
        //assina pra view os dados de usuario
        $this->view->assign('dados_usuario', $this->dados_usuario);
        //instancia para assinar os valores para o layout
        $this->layout = Zend_Layout::getMvcInstance();
    }

    /**
    * Tela inicial da conta do usuario, exibe os dados gerais
    */
    public function indexAction()
    {    	
        
    }

    /**
    * Lista todos os cupons disponiveis de uma categoria
    */
    public function produtosAction()
    {   
        //exibe todos os produtos dentro da data
        $desapega = $this->model_desapega->buscarTodosCuponsDentroDaData( $categoria, $this->dados_usuario->getId() );
        $this->view->assign('desapega', $desapega);
    }

    /**
    * Exibe os detalhes do produto e possibilidade de resgatar
    */
    public function detalheAction()
    {
        //verifica se foi passado o parametro id do cupom
        if( $this->getRequest()->getParam('id') ) {
            $idCupom = $this->getRequest()->getParam('id');

            //verifica se o participante já gerou este cupom
            if( $this->dados_usuario->getId() ) {
                $idUsuario = $this->dados_usuario->getId();
                if( $this->model_desapega->buscaRelacaoEntreCupomEParticipante($idUsuario, $idCupom) ){
                    $this->redirect('/');
                    exit;
                }
            }

            //assina para view os dados do cupom
            if( $idCupom ) {
                $dados = $this->model_desapega->buscarCupomOuCupomGerado( $idCupom );
                $this->layout->assign('detalhes', $dados);
                $this->view->assign('dados', $dados);
            }
        }

        //processo para gerar o cupom do participante
        if( $this->getRequest()->getParam('gerar') ) {
            $idCupom = $this->getRequest()->getParam('gerar');
            //se foi gerado com sucesso o cupom
            if($this->model_desapega->verificaQuantidadeDeCupomDisponivel( $idCupom )) {
                //se gerou o cupom
                if( $rand = $this->model_desapega->gerarCupom($this->dados_usuario, $idCupom) ) {
                    $this->redirect('/minhaconta/meucupom/rand/'.$rand.'/mensagem/sucesso');
                } else {
                    //caso tenha dado algum erro no momento de gravar o cupom exibe a mensagem de erro
                    $dados = $this->model_desapega->buscarCupomOuCupomGerado( $idCupom );
                    $this->view->assign('dados', $dados);
                    
                    $this->view->assign('mensagem', 'Não foi possível gerar o cupom. Tente novamente mais tarde');
                }
            } else {
                //caso tenha atingido o limite de cupons direciona para exibir a mensagem de erro
                $dados = $this->model_desapega->buscarCupomOuCupomGerado( $idCupom );
                $this->view->assign('dados', $dados);
                
                $this->view->assign('mensagem', 'Não foi possível gerar o cupom. O limite deste cupom foi atingido no momento, tente novamente mais tarde.');
            }
            
            
        }

        
    }

}


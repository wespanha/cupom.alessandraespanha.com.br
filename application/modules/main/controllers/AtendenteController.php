<?php

class Main_AtendenteController extends Zend_Controller_Action
{
    /**
    * armazena os dados do usuario logado no sistema
    */
    private $dados_usuario;

    /**
    * armazena o model com os processos do cupom
    */
    private $model_cupom;

    /**
    * armazena o model com os processos do atendente
    */
    private $model_atendente;

    /**
    * armazena a instacia do layout
    */
    private $layout;

    /**
    * armazena o formulario de login do atendente no sistema para validacao
    */
    private $form_loginatendente;

    public function init()
    {
        //busca dados registrados do usuario logado no sistema
        $this->dados_usuario = Zend_Auth::getInstance()->getStorage()->read();
        //model de cupons
        $this->model_cupom = new Main_Model_Cupom();
        //model atendente
        $this->model_atendente = new Main_Model_Atendente();
        //formulario para login do atendente
        $this->form_loginatendente = new Main_Form_Loginatendente();
        //assina pra view os dados de usuario
        $this->view->assign('dados_usuario', $this->dados_usuario);
        //instancia para assinar os valores para o layout
        $this->layout = Zend_Layout::getMvcInstance();
    }

    /**
    * Tela inicial da conta do usuario, exibe os dados gerais
    */
    public function indexAction()
    {    	
        //assina para view a mensagem de erro
        if( $this->getRequest()->getParam('mensagem') ) {
            $this->view->assign('mensagem', 'Não foi possível realizar o login. Verifique se o usuário e senha estão corretos, ou se o código pertence ao seu estabelecimento.');
        }

        //seta o codigo do cupom para o formulario de login para relacao do cupom, estabelecimento e atendente
        if( $this->getRequest()->getParam('rand') ) {
            $rand = $this->getRequest()->getParam('rand');
            $this->form_loginatendente->id->setValue($rand);
        } else {
            $this->redirect('/');
        }

        //formulário para login do atendente no sistema
        if( $this->getRequest()->isPost() ) {
            $form_data = $this->getRequest()->getPost();

            $auth = Plugins_Auth_AuthSetupAtendente::login($form_data['nomedeusuario'], $form_data['senha']);
            if( $auth ) {
                if( $dados = $this->model_atendente->loginAtendente( $form_data ) ) {
                    $this->view->assign('dados', $dados);
                } else {
                    $this->redirect('/atendente/index/rand/'.$form_data['id'].'/mensagem/erro');
                }
            }
            
        }

        $this->view->assign('form', $this->form_loginatendente);
    }

    /**
    * Processo de validacao do cupom
    */
    public function validarcupomAction()
    {
        if( $this->getRequest()->getParam('rand') ) {
            //se validou o cupom
            if( !$this->model_cupom->validarCupom( $this->getRequest()->getParam('rand') ) ) {
                $this->view->assign('mensagem', 'Não foi possível validar. Verifique se foi atingido o limite máximo de cupons ou se está vencido.');
            }
            //assina para a view os dados do cupom atualizado
            $cupom = $this->model_cupom->buscarCupomOuCupomGerado( null, $this->getRequest()->getParam('rand') );
            $this->view->assign('dados', $cupom);
        }
    }

}


<?php
// recebendo a url da imagem
$filename = $_GET['img'];
$img = explode('.', $_GET['img']);
//pega a extensão da imagem
$extensao = array_pop($img);

if( $extensao == 'jpeg' || $extensao == 'jpg' ) {

	$percent = 0.10;
	// Cabeçalho que ira definir a saida da pagina
	header('Content-type: image/jpeg');

	// pegando as dimensoes reais da imagem, largura e altura
	list($width, $height) = getimagesize($filename);

	//setando a largura da miniatura
	$new_width = $_GET['w'];
	//setando a altura da miniatura
	$new_height = $_GET['h'];

	//gerando a a miniatura da imagem
	$image_p = imagecreatetruecolor($new_width, $new_height);
	$image = imagecreatefromjpeg($filename);
	imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

	//o 3º argumento é a qualidade da imagem de 0 a 100
	imagejpeg($image_p, null, 100);
	imagedestroy($image_p);

} else if( $extensao == 'png' ) {

	$percent = 0.10;
	// Cabeçalho que ira definir a saida da pagina
	header('Content-type: image/png');

	// pegando as dimensoes reais da imagem, largura e altura
	list($width, $height) = getimagesize($filename);

	//setando a largura da miniatura
	$new_width = $_GET['w'];
	//setando a altura da miniatura
	$new_height = $_GET['h'];

	//gerando a a miniatura da imagem
	$image_p = imagecreatetruecolor($new_width, $new_height);
	$image = imagecreatefrompng($filename);
	imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

	imagepng($image_p);
	imagedestroy($image_p);
}
?>
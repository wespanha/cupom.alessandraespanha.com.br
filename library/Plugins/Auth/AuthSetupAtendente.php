<?php

class Plugins_Auth_AuthSetupAtendente
{

    public static function login($nomedeusuario, $senha)
    {
        $db = Zend_Registry::get('db');

        $adapter = new Zend_Auth_Adapter_DbTable($db);
        $adapter->setTableName('atendentes')
                ->setIdentityColumn('nomeDeUsuario')
                ->setCredentialColumn('senha')
                ->setCredentialTreatment('sha1(?)');

        $adapter->setIdentity($nomedeusuario)
                ->setCredential($senha);

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);

        $user = new Plugins_Auth_UserAcl();

        if ($result->isValid())
        {
            $data = $adapter->getResultRowObject();
            $user->setRole('atendente');
            $user->setId($data->id);
            $auth->getStorage()->write($user);
            return true;
        }else
            return false;
    }

}
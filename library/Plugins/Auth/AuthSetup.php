<?php

class Plugins_Auth_AuthSetup
{

    public static function login($email, $cpf)
    {
        $db = Zend_Registry::get('db');

        $adapter = new Zend_Auth_Adapter_DbTable($db);
        $adapter->setTableName('participantes')
                ->setIdentityColumn('email')
                ->setCredentialColumn('cpf');

        $adapter->setIdentity($email)
                ->setCredential($cpf);

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);

        $user = new Plugins_Auth_UserAcl();

        if ($result->isValid())
        {
            $data = $adapter->getResultRowObject();
            $user->setRole('participante');
            $user->setId($data->id);
            $user->setNome($data->nome);
            $user->setEmail($data->email);
            $user->setCpf($data->cpf);
            $user->setCelular($data->celular);
            $auth->getStorage()->write($user);
            return true;
        }else
            return false;
    }

}
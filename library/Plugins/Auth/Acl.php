<?php

class Plugins_Auth_Acl extends Zend_Controller_Plugin_Abstract 
{

    public function preDispatch(\Zend_Controller_Request_Abstract $request)
    {
        $auth = Zend_Auth::getInstance();
        $acl = Zend_Registry::get('acl');
        //se o usuário não autenticou o seu papel é guest
        if (!$auth->hasIdentity())
        {
            $user = new Plugins_Auth_UserAcl();
            $user->setRole('guest');
            $auth->getStorage()->write($user);
        }

        $role = $auth->getStorage()->read()->getRole();

        parent::preDispatch($request);
        
        $module     = $request->getModuleName();
        $controller = $request->getControllerName();
        $action     = $request->getActionName();
        
        $resource = "{$module}:{$controller}:{$action}";

        if( $resource == 'admin:index:index' && $acl->isAllowed($role, 'admin:dashboard:index', 'index') ) {
            $request->setModuleName('admin');
            $request->setControllerName('dashboard');
            $request->setActionName('index');
        }

        if( $resource == 'main:minhaconta:index' && $acl->isAllowed($role, 'main:minhaconta:detalhe', 'detalhe') ) {
            $request->setModuleName('main');
            $request->setControllerName('minhaconta');
            $request->setActionName('detalhe');
        }

        if ( !$acl->isAllowed($role, $resource, $action) ) {
            $request->setModuleName('main');
            $request->setControllerName('error');
            $request->setActionName('denied');
        }

    }
}

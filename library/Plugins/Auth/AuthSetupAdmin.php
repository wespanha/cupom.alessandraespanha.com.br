<?php

class Plugins_Auth_AuthSetupAdmin
{

    public static function login($email, $senha)
    {
        $db = Zend_Registry::get('db');

        $adapter = new Zend_Auth_Adapter_DbTable($db);
        $adapter->setTableName('admin_usuario')
                ->setIdentityColumn('email')
                ->setCredentialColumn('senha')
                ->setCredentialTreatment('sha1(?)');

        $adapter->getDbSelect()->where("status = 's'");

        $adapter->setIdentity($email)
                ->setCredential($senha);

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);

        $user = new Plugins_Auth_UserAcl();

        if ($result->isValid()) {

            $data = $adapter->getResultRowObject();
            $user->setRole($data->permissao);
            $user->setId($data->id);
            $user->setNome($data->nome);
            $user->setEmail($data->email);
            $auth->getStorage()->write($user);
            return true;

        } else {

            return false;

        }
    }

}